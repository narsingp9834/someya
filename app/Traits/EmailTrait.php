<?php
namespace App\Traits;
use Illuminate\Http\Request;


trait EmailTrait {

    public function getRegisterEmailTemp($details,$password)
    {
        $content = "Welcome ".$details->user_fname." ".$details->user_lname.", <br><br>

        Your Weekley Electric account has been created successfully! Please use the below credentials to log into your app. <br><br>
        
        Your Login Info:<br>
        Email: ".$details->email."<br>
        Password: ".$password."<br>
        ";
        return $content;
    }
}




?>