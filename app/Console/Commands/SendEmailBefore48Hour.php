<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\Customer;
use App\Models\Generator;
use App\Models\Holiday;
use App\Models\LogCron;
use App\Models\OrganizationSetting;
use App\Models\Specialization;
use App\Models\User;
use App\Traits\CommonTrait;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SendEmailBefore48Hour extends Command
{
    use CommonTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointment:before48-hours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email reminders 48 hours before appointment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
            try {
                \Log::info("48 Hour Reminder Cron Started");
                $todaysDate = today()->format('Y-m-d');
                $orgSetting = $this->getOrgSettings(1);
                $hours = $orgSetting->reminder_sent_before_hours;
                $timeZone = $orgSetting->timezone;
                $hoursArray = explode(',',$hours);
                $appointments = Appointment::select('appointments.*','customers.customer_id','users.id as user_id','slots.slot_time')->join('customers','customers.customer_id','appointments.customer_id')
                ->join('users','users.id','customers.user_id')
                ->join('slots','slots.slot_id','appointments.slot_id')
                ->whereIn('appointments.service_id',['1','2'])
                ->where('appointments.slot_date', '>=', $todaysDate)->where('appointments.appointment_status','Approved')->get();
                
                    foreach ($appointments as $appointment) {

                        for ($i=0; $i < count($hoursArray); $i++) { 
                            $slotData = explode('-', $appointment->slot_time);
                            $slotTime = $slotData[0];
            
                            $service     = Specialization::find($appointment->service_id);
                            $generator   = Generator::find($appointment->generator_id);
                            $appointmentData['request_type'] = $service->name;
                            $appointmentData['genarator'] = null;
                            if($generator != null){
                                $appointmentData['genarator']    = $generator->generator_name;
                            }
            
                            $created_at = $appointment->created_at;
            
                            $timeInLocal =  $created_at->format('h:ia');
                            $dateInLocal =  $created_at->format('m/d/Y');
                            $appointmentData['current_time'] = $timeInLocal;
                            $appointmentData['current_date'] = $dateInLocal;
                            $appointmentData['slot_date']    = $appointment->slot_date;
                            $appointmentData['slot_time']    = $slotTime;
                            $appointmentData['appointment_id'] = $appointment->appointment_id;
                            $appointmentData['short_link'] = $this->generateUniqueShortUrl();
                            $appointmentData['confirm_link'] = config('constants.app_url').'appointment/confirm/'.encrypt($appointment->appointment_id);
                            $appointmentData['between_slot_time'] = $appointment->slot_time;

                            $currentDate = new \DateTime('now',new \DateTimeZone("UTC")); 
                            $appointmentDateTime = $appointment->slot_date. ' ' . $slotTime;
                            $carbon = Carbon::parse($appointmentDateTime, $timeZone);

                            // Convert the date and time to UTC
                            $utcDateTime = $carbon->utc();

                            $holidayDates = $this->getHolidays(1);

                            $workingDayDifference = 0;
                            $dateIterator = clone $currentDate;
                            //$dateIterator = $dateIterator->add(new \DateInterval('P1D'));
                            while ($dateIterator < $utcDateTime) {
                                if (!($this->isWeekend(Carbon::instance($dateIterator))) && !($this->isHoliday(Carbon::instance($dateIterator), $holidayDates))) {
                                    $workingDayDifference++;
                                }
                                $dateIterator->add(new \DateInterval('P1D'));
                            }
                            
                            \Log::info('Days Difference '.$workingDayDifference);
                            $reminderSentArray = explode(',',$appointment->is_reminder_email_sent);
                            if ((in_array($workingDayDifference,$hoursArray)) && ($reminderSentArray[$i] != 'Yes')) {
                                    $logData = $this->addCronLog($appointment->appointment_id,'Reminder for appointment');
                                    if($logData !== false){
                                        try {
                                        $user = User::find($appointment->user_id);
                                        $customer = Customer::find($appointment->customer_id);
                                        $this->sendEmail($user,$customer,'48_hours_before_appointment',$appointmentData);
                                        $this->sendAptSNS($user,'48_hours_before_appointment',$appointmentData);
                                        $this->updateCronLog($logData);
                                        
                                        $reminderSentArray[$i] = 'Yes';
                                        $reminderData = implode(',',$reminderSentArray);
                                        Appointment::where('appointment_id',$appointment->appointment_id)->update(['is_reminder_email_sent'=>$reminderData]);
                                            
                                        } catch (\Exception $e) {
                                            $this->updateCronLog($logData, $e->getMessage());
                                            \Log::error($e->getMessage());

                                            $reminderSentArray[$i] = 'No';
                                                $reminderData = implode(',',$reminderSentArray);
                                                Appointment::where('appointment_id',$appointment->appointment_id)->update(['is_reminder_email_sent'=>$reminderData]);
                                        }
                                    }
                      
                                
                            }
                        }
                   
    
                    }
                
            } catch (\Exception $e) {

                \Log::error($e->getMessage());
            }
    }
}
