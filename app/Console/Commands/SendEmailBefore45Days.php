<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\Customer;
use App\Models\Generator;
use App\Models\OrganizationSetting;
use App\Models\Specialization;
use App\Models\User;
use App\Traits\CommonTrait;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SendEmailBefore45Days extends Command
{
    use CommonTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maintenance:before45-days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email reminders 45 days before Maintenance Due Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("45 Days Reminder Cron Started");
            $orgSetting = $this->getOrgSettings(1);
            $days = $orgSetting->reminder_sent_before_days;
            $daysArray = explode(',',$days);
            $timeZone = $orgSetting->timezone;
            $appointments = Appointment::select('appointments.*','customers.customer_id','users.id as user_id')->join('customers','customers.customer_id','appointments.customer_id')
            ->join('users','users.id','customers.user_id')
            ->join('slots','slots.slot_id','appointments.slot_id')
            ->where(['service_id'=>1, 'appointment_status'=>'Completed'])->get();
            foreach ($appointments as $appointment) {
                    for ($i=0; $i < count($daysArray); $i++) { 
                                $service     = Specialization::find($appointment->service_id);
                                $generator   = Generator::find($appointment->generator_id);
                                $appointmentData['request_type'] = $service->name;
                                $appointmentData['genarator']    = $generator->generator_name;
                                $utc = $appointment->created_at;
                                $created_at = new \DateTime($utc, new \DateTimeZone("UTC"));

                                $timeInLocal =  $created_at->format('h:ia');
                                $dateInLocal =  $created_at->format('m/d/Y');
                                $appointmentData['current_time'] = $timeInLocal;
                                $appointmentData['current_date'] = $dateInLocal;
                                $appointmentData['slot_date'] = $appointment->slot_date;
                                $appointmentData['slot_time'] = null;
                                // Get the current date
                                $currentDate = new \DateTime('now',new \DateTimeZone("UTC")); 

                                $carbon = Carbon::parse($appointment->slot_date, $timeZone);

                                // Convert the date and time to UTC
                                $givenDate = $carbon->utc();

                                // $givenDate = new \DateTime($appointment->slot_date,new \DateTimeZone("UTC")); 
                                $givenDate->setTime(0, 0, 0);
                                if($generator->maintenance_package == "6") {
                                    $givenDate->add(new \DateInterval('P6M'));
                                }

                                if($generator->maintenance_package == "12") {
                                    $givenDate->add(new \DateInterval('P12M'));
                                }

                                $dateInterval = $currentDate->diff($givenDate);
                                $differenceInDays = $dateInterval->days;  
                                $reminderSentArray = explode(',',$appointment->is_maintenance_email_sent);
                                if (in_array($differenceInDays,$daysArray) && ($reminderSentArray[$i] != 'Yes')) {
                                        $logData = $this->addCronLog($appointment->appointment_id,'Reminder for generator maintenance');
                                        if($logData !== false){
                                            try {
                                                $appointmentData['genarator']    = $generator->generator_name;
                                                $user = User::find($appointment->user_id);
                                                $customer = Customer::find($appointment->customer_id);
                                                $this->sendEmail($user,$customer,'45_days_before_maintenance',$appointmentData);
                                                $this->sendAptSNS($user,'45_days_before_maintenance',$appointmentData);
                                                $this->updateCronLog($logData);

                                                $reminderSentArray[$i] = 'Yes';
                                                $reminderData = implode(',',$reminderSentArray);
                                                Appointment::where('appointment_id',$appointment->appointment_id)->update(['is_maintenance_email_sent'=>$reminderData]);

                                            } catch (\Exception $e) {
                                                $this->updateCronLog($logData, $e->getMessage());
                                                \Log::error($e->getMessage());

                                                $reminderSentArray[$i] = 'No';
                                                $reminderData = implode(',',$reminderSentArray);
                                                Appointment::where('appointment_id',$appointment->appointment_id)->update(['is_maintenance_email_sent'=>$reminderData]);
                                            }
                                        }
                         

                                }
                    }
                
                }
            }
    }
