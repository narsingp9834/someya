<?php

namespace App\Http\Controllers;

use App\Models\CMSProduct;
use App\Models\ContactUs;
use App\Models\QuickLink;
use Illuminate\Http\Request;
use DataTables;

class QuickLinkController extends Controller
{
    public function __construct()
    {
        $this->title = "Quick Links";
        $this->middleware('auth');
    }

    public function index(Request $request) {
		try {
            if ($request->ajax()) {
                $data = QuickLink::all();

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->link_id);
                    
                    $btn = "
                    <a href='".url('/quick-link/edit/'.$id)."' class='item-edit text-dark' title='Edit Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='QuickLink' title='Delete Quick Link'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','Status'])
                ->make(true);
            }

            $title = 'Quick Link';
            return view('quick-link.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}

    public function create() {
        $title = $this->title;
        return view('quick-link.create',compact('title'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
            
            QuickLink::create($input);
            toastr()->success("QuickLink Created Successfully");
            return redirect('quick-link');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('quick-link/create');
        }
    }

    public function edit($planId) {
        try {
            $title = $this->title;
            $link = QuickLink::find(decrypt($planId));
            return view('quick-link.edit',compact('title','link'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('quick-link');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();
            $searchInput['link_id'] = $input['link_id'];

            QuickLink::updateOrcreate($searchInput, $input);
            toastr()->success("QuickLink Updated Successfully");
            return redirect('quick-link');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('quick-link');
        }
    }

    
    public function view($id) {
        try {
            $title = $this->title;
            $link = QuickLink::find($id);
            $products = CMSProduct::all();
            $links   = QuickLink::all();
            return view('quick-link.view',compact('title','link','products','links'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('cms-product');
        }
    }

    public function Enquiry(Request $request) {
		try {
            if ($request->ajax()) {
                $data = ContactUs::join('cms-products','cms-products.product_id','contact_us.category_id');

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->link_id);
                    
                    $btn = "
                    <a href='".url('/quick-link/edit/'.$id)."' class='item-edit text-dark' title='Edit Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='QuickLink' title='Delete Quick Link'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','Status'])
                ->make(true);
            }

            $title = 'Enquiry';
            return view('enquiry',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}
}
