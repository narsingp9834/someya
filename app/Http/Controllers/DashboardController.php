<?php

namespace App\Http\Controllers;

use App\Models\CMSProduct;
use App\Models\UserPlanRequest;
use Illuminate\Http\Request;
use Auth;
use DataTables;
use DB;
use Illuminate\Support\Carbon;
use App\Traits\CommonTrait;
use App\Models\Banner;
use App\Models\ContactUs;
use App\Models\CustomerBanner;
use App\Models\QuickLink;

class DashboardController extends Controller
{

	use CommonTrait;

	public function showHomePage() {
		$products = CMSProduct::all();
		$banners  = Banner::all();
		$customerBanners  = CustomerBanner::all();
		$links         = QuickLink::all();

		return view('homepage',compact('products','banners','customerBanners','links'));
	}

	public function ourQuality() {
		$products = CMSProduct::all();
		$links         = QuickLink::all();

		return view('our-quality',compact('products','links'));
	}

	public function whyUs() {
		$products = CMSProduct::all();
		$links         = QuickLink::all();

		return view('why-us',compact('products','links'));
	}
	

	public function submitContactUs(Request $request) {
		$input = $request->all();
		ContactUs::create($input);
		toastr()->success('Form Submitted Successfully');
		return redirect()->back();
	}

	public function Enquiry(Request $request) {
		try {
            if ($request->ajax()) {
                $data = ContactUs::all();

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
				->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->contact_us_id);
                    $btn = " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='ContactUs' title='Delete'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','Status'])
                ->make(true);
            }

            $title = 'Enquiry';
            return view('enquiry',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}

	public function banner(Request $request) {
		try {
            if ($request->ajax()) {
                $data = Banner::all();

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('image', function ($row){
					return "<img src='$row->image' style='width: 150px;height: 80px;'>";
				})
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
				->addColumn('action', function ($row) {
                    $id  = encrypt($row->banner_id);

                    $btn = " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Banner' title='Delete Invoice'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
				->rawColumns(['image','action'])
                ->make(true);
            }

            $title = 'Banner';
            return view('banner',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}

	public function bannerStore(Request $request) {
		$imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
		$input['image'] = env('APP_URL').'/public/images/'.$imageName;
		Banner::create($input);
		toastr()->success('Banner Created Successfully');
		return redirect()->back();
	}


	public function customerBanner(Request $request) {
		try {
            if ($request->ajax()) {
                $data = CustomerBanner::all();

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('image', function ($row){
					return "<img src='$row->image' style='width: 150px;height: 80px;'>";
				})
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
				->addColumn('action', function ($row) {
                    $id  = encrypt($row->banner_id);

                    $btn = " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Banner' title='Delete Invoice'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
				->rawColumns(['image','action'])
                ->make(true);
            }

            $title = 'Banner';
            return view('customer-banner',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}

	public function customerBannerStore(Request $request) {
		$imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
		$input['image'] = env('APP_URL').'/public/images/'.$imageName;
		CustomerBanner::create($input);
		toastr()->success('Banner Created Successfully');
		return redirect()->back();
	}

	public function contactUs() {
		$title = 'Contact Us';
		$products = CMSProduct::all();
		$links    = QuickLink::all();
		return view('contact-us',compact('products','links'));
	}
}