<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\AddressType;
use Illuminate\Http\Request;
use App\Traits\CommonTrait;
use App\Models\State;
use App\Models\City;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Generator;
use App\Models\NavigationMenu;
use App\Models\RoleHasMenu;
use App\Models\SubMenu;
use App\Models\TechnicianServiceDetail;
use App\Models\User;
use App\Models\UserRole;
use Auth;

class CommonController extends Controller
{
    use CommonTrait;

    public function __construct(){
        if(Auth::check() == true){
            $this->middleware('auth');
            $this->middleware(function ($request, $next) {
                $this->user = Auth::user();
                return $next($request);
            });
        }
    }
 	
 	  // Delete Records
 	public function destroyRecord(Request $request)
  	{    
        $id = decrypt($request->id);
        
  		$this->deleteRecord($request, $request->model, 'deleted_at');
  	}

  	// Update Status
  	public function updateStatus(Request $request){
        $this->modifyStatus($request, $request->model, $request->field);
  	}

    // Common function for get states according to country id
    public function getStates(Request $request)
    {
        if (isset($request->state_id) && $request->state_id != '') {
            $states = State::where(['country_id' => $request->id])->get();
        } else {
            $states = State::where(['country_id' => $request->id, 'state_status' => 'Active'])->get();
        }
        echo json_encode($states);
        exit;
    }

    // Common function for get cities according to state id
    public function getCities(Request $request)
    {
        if (isset($request->city_id) && $request->city_id != '') {
            $cities =City::where(['state_id' => $request->id])->get();
        } else {
            $cities = City::where(['state_id' => $request->id, 'city_status' => 'Active'])->get();
        }
        echo json_encode($cities);
        exit;
    }
}