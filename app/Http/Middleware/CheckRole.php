<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
                // Get the required roles from the route
                $roles = $this->getRequiredRoleForRoute($request->route());

                if(in_array(auth()->user()->user_type, $roles) || !$roles){
                    return $next($request);
                }
        
                return Redirect('/not-authorized');
        return $next($request);
    }

    private function getRequiredRoleForRoute($route){
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }
}
