<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CMSProduct extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'content',
        'image',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $primaryKey = 'product_id';
    public $table = 'cms-products';
}
