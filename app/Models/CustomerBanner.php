<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerBanner extends Model
{
    use HasFactory;
    protected $fillable = [
        'image',
        'created_at',
        'updated_at',
    ];

    public $primaryKey = 'customer_banner_id';
    public $table = 'customer_banner';
}
