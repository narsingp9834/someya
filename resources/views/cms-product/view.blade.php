<!--=========================== ===============================-->
    
<html>

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Product|SOMEYA</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('web_assets/images/someyalogo.jpeg')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-style.css')}}">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
{{-- <style>
    .card-section{
    margin-top: 70px;
    
}
.card-section-sc{
   background-color: #fff;
   padding: 25px 25px;
   border-radius: 10px;
    
}
.card-left-sc{
    padding: 10px 25px;
    
}
.card-right-sc{
    border-radius: 25px;
    padding: 10px 25px;
}
.img-right{
    border-radius: 25px;
}
.card-section-heading{
    margin-top: 5rem;
    margin-bottom: 25px;
    border-bottom: 2px solid #1967d2;
}
.card-left-sc h6{
   padding: 5px 10px;
   border-radius: 5px;
    background-color: #1967d2;
    color: #fff;
    font-size: 14px;
    width: fit-content;
    margin-bottom: 25px;
}
.card-left-sc p{
    color: #202124;
    font-size: 15px;
}

</style> --}}

<body>
    
    <!---======================Header Start Here =====================================-->
    @include('common.header',['products'=>$products])
    <!---======================Header End Here =====================================-->

<div class="card-section mt-5">
        

    <div class="container">
        
        <div class="row card-section-sc">
            <div class="col-lg-6 col-md-6 mt-10">
                <div class="card-left-sc">
                    <h6>{{$product->name}}
                    </h6>
                    <p>{{$product->content}}</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 mt-10">
                    <div class="card-right-sc">
                        <img src="{{ $product->image}}" class="d-block w-100 img-right" alt="...">
                    </div>
            </div>
                       

        </div>
    </div>
</div>

    <!--=========================== Footer Start Here ===============================-->
    @include('common.footer',['products'=>$products,'links'=>$links])
    <!---======================Footer End Here =====================================-->


    <!---======================Sub Footer Start Here =====================================-->
    {{-- <section class="sub-footer-sc">
        <p>© 2024 data . All Right Reserved.</p>
    </section> --}}
    <!---======================Sub Footer End Here =====================================-->
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>

<!--==================== Footer Start Here ===================================-->
{{-- <section class="footer-section mt-5">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-logo-sc" style="padding: 0px 17px;">
                        <img src="{{asset('assets/images/someyalogo.jpeg')}}" width="70px" style="border-radius: 50%;"
                            class="mb-3"></a>
                    </div>
                    <p style="color: #FFFFFFA1;font-size: 14px;">We are a uniform manufacturer, uniform wholesaler
                        and uniform supplier of superlative quality uniforms and workwear, providing uniform to
                        diverse range of industries like School Dress
                        sectors.
                    </p>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-link-sc">
                        <h5 class="footer-link-main-heading">For Candidates </h5>
                        <ul>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Browse Jobs</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Browse Candidates</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Candidate Dashboard</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Job Alerts</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> My Bookmarks</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-link-sc">
                        <h5 class="footer-link-main-heading">For Employers </h5>
                        <ul>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> All Employers</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Employer Dashboard</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Submit Job</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Job Packages</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Contact Us</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-link-sc">
                        <h5 class="footer-link-main-heading">Helpful Resources </h5>
                        <ul>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Site Map</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Terms of Use</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Privacy Center</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Security Center</a></li>
                            <li><a href="#"><i class="bi bi-arrow-right-short"></i> Accessibility Center</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer-line"></div>
                <span class="reserved-text">© 2023 Sona Signature LLP. All rights reserved.</span>
            </div>
        </div>
    </footer>
</section> --}}
<!--==================== Footer End Here ===================================-->