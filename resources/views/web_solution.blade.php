<!--=========================== ===============================-->
    
<html>

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> | Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
    .card-section{
    margin-top: 70px;
    
}
.card-section-sc{
   background-color: #fff;
   padding: 25px 25px;
   border-radius: 10px;
    
}
.card-left-sc{
    padding: 10px 25px;
    
}
.card-right-sc{
    border-radius: 25px;
    padding: 10px 25px;
}
.img-right{
    border-radius: 25px;
}
.card-section-heading{
    margin-top: 5rem;
    margin-bottom: 25px;
    border-bottom: 2px solid #1967d2;
}
.card-left-sc h6{
   padding: 5px 10px;
   border-radius: 5px;
    background-color: #1967d2;
    color: #fff;
    font-size: 14px;
    width: fit-content;
    margin-bottom: 25px;
}
.card-left-sc p{
    color: #202124;
    font-size: 15px;
}

</style>

<body>
    
    <!---======================Header Start Here =====================================-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="{{asset('images/new_logo1.jpeg')}}" class="img-fluid"
                    width="140px"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar"
                aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Logo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-start flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link" href="homepage.php">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Careers</a>
                        </li>
                        
                         <li class="nav-item">
                            <a class="nav-link" href="#">Tax Blog</a>
                        </li>

                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('category/products/2')}}">Best Web Solutions</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Invoice Software</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Web Learning</a>
                        </li>
                       
                    </ul>

                    <form class="d-flex">

                        <a href="{{url('seller/login')}}" class="btn custom-btn ">Free Demo for 7 days</a>
                        {{-- <a href="signup.html" class="btn custom-btn signup">Sign Up</a> --}}

                    </form>
                </div>
            </div>
        </div>
    </nav>
    <!---======================Header End Here =====================================-->

<div class="card-section ">
        

    <div class="container">
        <h3 class="card-section-heading">INVOICE SOFTWARE</h3>
        
        <div class="row card-section-sc">
            <div class="col-lg-6 col-md-6">
                <div class="card-left-sc">
                    <h6>Invoicing
                    </h6>
                    <p>Maintain a full sales pipeline by continuous prospecting through the generation of custom
                        quotes
                        and invoices. Generate quotes in just a few minutes to save your valuable time</p>
                    <p>Simply convert the quotes directly into invoice and send to clients after their approval.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card-right-sc">
                    <div id="carouselExample" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/images/main.png" class="d-block w-100 img-right" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/images/main.png" class="d-block w-100 img-right" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample"
                            data-bs-slide="prev">
                            <span class="" aria-hidden="true"><i class="bi bi-arrow-left-circle" style="color: #1967d2;font-size: 25px;"></i></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample"
                            data-bs-slide="next">
                            <span class="" aria-hidden="true"><i class="bi bi-arrow-right-circle" style="color: #1967d2;font-size: 25px;"></i></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--=========================== ===============================-->

<!--=========================== ===============================-->

<div class="card-section ">
    

    <div class="container">
        <h3 class="card-section-heading">BEST WEB SOLUTIONS</h3>
        
        <div class="row card-section-sc">
            <div class="col-lg-6 col-md-6">
                <div class="card-left-sc">
                    <h6>Details

                    </h6>
                    <p>Maintain a full sales pipeline by continuous prospecting through the generation of custom
                        quotes
                        and invoices. Generate quotes in just a few minutes to save your valuable time</p>
                    <p>Simply convert the quotes directly into invoice and send to clients after their approval.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card-right-sc">
                    <div id="carouselExample2" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                               <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                            <div class="carousel-item">
                                 <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('images/main.jpeg')}}" class="d-block w-100 img-right" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample2"
                            data-bs-slide="prev">
                            <span class="" aria-hidden="true"><i class="bi bi-arrow-left-circle" style="color: #1967d2;font-size: 25px;"></i></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample2"
                            data-bs-slide="next">
                            <span class="" aria-hidden="true"><i class="bi bi-arrow-right-circle" style="color: #1967d2;font-size: 25px;"></i></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--=========================== ===============================-->



    <!--=========================== Footer Start Here ===============================-->
    <section class="footer-sc">>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="img-section">
                            <h4 class="footer-link-heading"><img src="{{asset('images/new_logo1.jpeg')}}" class="img-fluid"
                                    width="180px"></h4>
                            <div class="footer-logo">

                            </div>
                            <p class="footer-logo-disclaimer">
                                Duis mattis laoreet neque, et ornare neque sollicitudin at. Proin sagittis dolor sed mi
                                elementum pretium. Donec et justo ante.

                            </p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="link-section">
                            <h5 class="footer-link-heading">For Candidates</h5>
                            <ul>
                                <li><a href="{{url('login')}}">Staff Login </a></li>
                                <li><a href="{{url('login')}}">Buyer login</a></li>
                                <li><a href="{{url('contact-us')}}">Contact Us</a></li>
                                <li><a href="#"> Candidate Dashboard </a></li>
                                <li><a href="#"> Job Alerts </a></li>
                                <li><a href="#"> My Bookmarks </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="link-section">
                            <h5 class="footer-link-heading">For Employers </h5>
                            <ul>
                                <li><a href="#"> Browse Candidates </a></li>
                                <li><a href="#"> Employer Dashboard </a></li>
                                <li><a href="#"> Add Job </a></li>
                                <li><a href="#"> Job Pakages </a></li>
                                <li><a href="#"> Browse Candidates </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="link-section">
                            <h5 class="footer-link-heading">Helpful Resources </h5>
                            <ul>
                                <li><a href="#"> Site Map </a></li>
                                <li><a href="#"> Terms of Use </a></li>
                                <li><a href="#"> Privacy Center </a></li>
                                <li><a href="#"> Security Center </a></li>
                                <li><a href="#"> Accessibility Center </a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </footer>
    </section>
    <!---======================Footer End Here =====================================-->


    <!---======================Sub Footer Start Here =====================================-->
    <section class="sub-footer-sc">
        <p>© 2024 data . All Right Reserved.</p>
    </section>
    <!---======================Sub Footer End Here =====================================-->
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>