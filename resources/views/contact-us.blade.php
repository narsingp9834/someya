<!--=========================== ===============================-->

<html>

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Product|SOMEYA</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('web_assets/images/someyalogo.jpeg') }}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-style.css')}}">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom-style.css') }}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
    .card-section {
        margin-top: 70px;

    }

    .card-section-sc {
        background-color: #fff;
        padding: 25px 25px;
        border-radius: 10px;

    }

    .card-left-sc {
        padding: 10px 25px;

    }

    .card-right-sc {
        border-radius: 25px;
        padding: 10px 25px;
    }

    .img-right {
        border-radius: 25px;
    }

    .card-section-heading {
        margin-top: 5rem;
        margin-bottom: 25px;
        border-bottom: 2px solid #1967d2;
    }

    .card-left-sc h6 {
        padding: 5px 10px;
        border-radius: 5px;
        background-color: #1967d2;
        color: #fff;
        font-size: 14px;
        width: fit-content;
        margin-bottom: 25px;
    }

    .card-left-sc p {
        color: #202124;
        font-size: 15px;
    }
</style>

<body>

    <!---======================Header Start Here =====================================-->
    @include('common.header', ['products' => $products])
    <!---======================Header End Here =====================================-->

    <!--==================== who we are Start Here ===================================-->
    <section class="who-we-are-sc mt-5">
        <div class="container">
            <h4 class="landing-page-main-heading">HAVE YOU GOT A UNIFORM VISION?
            </h4>
            <p class="landing-page-sub-heading">Looking for a new supplier you can trust? If you are looking for
                quality, <br> durability, and top-notch designs, look no further.
                Get in touch with us today!
            </p>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div class="text-center">
                        <img src="{{asset('assets/images/mail.png')}}" width="65%" class="img-fluid">
                    </div>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-7"
                    style="padding: 20px 45px;background-color: #fff3cd;border-radius: 10px;">
                    <form class="row g-2" action="{{url('submit-contact-us')}}" method="POST">
                        @csrf
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label class="form-label"> Full Name</label>
                                <input type="text"  name="full_name" id="full_name" class="form-control form-control-custom" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label class="form-label"> Phone</label>
                                <input type="text" name="phone_no" id="phone_no" class="form-control form-control-custom" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label class="form-label"> City</label>
                                <input type="text" name="city" id="city" class="form-control form-control-custom" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label"> Category</label>
                            <select class="form-select" name="category_id" id="category_id" style="height: 39px;border-radius: 10px;" required>
                                <option value="">Select Category</option>
                                @foreach ($products as $product)
                                        <option value="{{$product->product_id}}">{{$product->name}}</option>
                                    @endforeach
                            </select>
                        </div>
    
                        <div class="col-12">
                            <label for="exampleFormControlTextarea1" class="form-label">Pieces</label>
                            <input type="text" name="message" id="message" class="form-control form-control-custom" required>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn custom-btn btn-customs">Submit</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </section>
    <!--==================== who we are end Here ===================================-->

    <!--=========================== Footer Start Here ===============================-->
    @include('common.footer', ['products' => $products, 'links' => $links])
    <!---======================Footer End Here =====================================-->


    <!---======================Sub Footer Start Here =====================================-->
    {{-- <section class="sub-footer-sc">
        <p>© 2024 data . All Right Reserved.</p>
    </section> --}}
    <!---======================Sub Footer End Here =====================================-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
