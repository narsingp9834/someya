@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap.min.css')) }}">
@endsection
@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">

    <style>
            .span-design {

            margin-top: -15px;


            }
           

            .dashboard {
            font-size: 25px;
            }

            .inner p {
            font-size: 18px;
            margin-top: -10px;
            }

            .inner h3 {
            font-size: 25px !important;
            }

            .small-box {
                border-radius: .25rem;
                box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
                display: block;
                margin-bottom: 20px;
                position: relative;
            }
            .bg-info {
                background-color: #17a2b8 !important;
            }
            .bg-info, .bg-info>a {
                color: #fff !important;
            }
            .small-box>.inner {
                padding: 10px;
            }
            .small-box .icon {
                color: rgba(0, 0, 0, .15);
                z-index: 0;
            }

            .small-box>.small-box-footer {
                background-color: rgba(0, 0, 0, .1);
                color: rgba(255, 255, 255, .8);
                display: block;
                padding: 3px 0;
                position: relative;
                text-align: center;
                text-decoration: none;
                z-index: 10; 
            }
            .time-sc{
                float:right;margin-top:-55px;background-color:#ed3240;color:#fff;padding:4px 10px;border-radius:4px
            }
    </style>
@endsection

@section('content')

   
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js" integrity="sha512-f6ARr8ILY9YDtYq8Kpl3je9bFh1xvqTtDKA+jZL5OxYbQ7HhEFL8BYY3M2eJrLb/6WvSvE8xGnXJpZQLaJSv5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="{{ asset('js/dashboard.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection
