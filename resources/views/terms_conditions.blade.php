<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Terms & Conditions|SASTA AUR SECURE HAI BOSS
        APNA DIGITAL EKHATABOOK</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    @include('common.header')
     <!---======================Banner Start Here =====================================-->

      <!---====================== Banner End Here=====================================-->
    


 

        <div class="container" style="margin-top: 6rem;background-color:#fff;padding:45px 45px">
            <h4 class="home-page-main-heading " style="color: #ff4343; padding: 5px 12px; margin-bottom: 25px;">
            Terms and Conditions</h4>
                    
                
                    <p>he services(“Services”) are provided by E- STORE SOLUTION. All the services and applications(products) provided to you, by or through Ekhatabook Website are subjected to these Terms and Conditions of use. We recommend you to read these Terms and Conditions carefully and completely, before using Ekhatabook’s Services. Using or accessing the Ekhatabook Services, reflects that you stick to these terms of use. You are advised not to use the Ekhatabook services, if you do not agree with these Terms of services.</p>
                    <h6>1. Account Terms</h6>
                    <p>To use Ekhatabook’s service you must be at-least thirteen(13) years of age and above.
                    You must agree to these Terms of Service, to use our applications(products) and services.
                    You must have to provide accurate, current, valid information (full Legal name and Valid Email-id) and should complete the registration process.
                    By registering into Ekhatabook’s service, you are entering into an agreement with E- Store Solution, that you should keep following and stay trustworthy to Ekhatabook.
                    In case of violating any of our conditions, your account may be terminated.</p>
                    <h6>2. Registration Data , Account Security</h6>
                    <p>By registering on Ekhatabook’s services, you agree to</p>
                     
                    <p>•	Terms and conditions of the Ekhatabook Software Services.</p>
                    <p>•	Provide accurate, current, complete and valid information on any registration forms (“Registration Data”)on the Service.</p>
                    <p>•	Maintain your password and your API key by E- Store Solution.</p>
                    <p>•	Stay responsible for all your Sensitive data, Ekhatabook’s Account and other details you use through Ekhatabook Services.</p>
                    <p>•	Takeover all the risk based on unauthorized access to the registered data.</p>
                    <p>•	Termination of your account at any time by us, when offending the Terms of Ekhatabook.</p>
                    <p>We will provide you non-exclusive, non-transferable, limited access to you, during your access. Your use of Ekhatabook Services are at your own risk. You are responsible to maintain your password confidential.
                    <h6>3. Your Use of the Service</h6>
                    <p>By accessing or using Ekhatabook services, you agree to follow these terms. Even if you use this service on behalf of your organization, you are also assuring that you agree to follow these Terms. You can use the Services offered by Ekhatabook, only if you bind a contract with Ekhatabook. In case of any changes or holding a service temporarily or permanently, with our services, you will be intimated by prior notice. You are advised not to misuse the service. Accessing or using our service, doesn’t gives you the ownership on the intellectual property on the content or the services.</p>
                    <h6>4. Payment Terms / Refund Policy</h6>
                    <p>You are responsible for all taxes applicable to the fees and charges in any applicable jurisdiction. Ekhatabook will offer minimum Seven (7) days free trial for all our products/services to newly registered users. You are requested to purchase our software only after using the trial version.
                    For On-Premises products/services, after Purchase the license key will be issued. Once the license key is delivered, there is no Refund option.
                    For SaaS hosted services, Your payments should be in advance. You can use either of these plans.
                    Monthly Plan : All of Ekhatabook’s services are billed in advance and is non refundable. Therefore you shouldn’t expect for refunds, partial months credits, months unused, account upgrades or downgrades.
                    Annual Plan : Refunds can be applicable from the unused months to the date of cancellation.
                    The Fees for the Services or products provided by Ekhatabook, as displayed on the Ekhatabook/our  Products website, are subject to change. Ekhatabook reserves the right to modify the Fee at any time with a notification to the User. In case of the User’s continued use of the Ekhatabook Products, it shall be deemed that the User has accepted such revised Fees.</p>
                    <h6>5.License</h6>
                    <p>The license provided to you is non-transferable and non-exclusive.The license given to you is in accordance with the terms of service.Your license cannot be transferred to any other party under any circumstances. You should not share your account login information to anyone outside your organization.</p>
                    <h6>6. Cancellation and Termination</h6>
                   <p> You can cancel your license/usage of your account at any time, but you alone are responsible for proper cancellation. If you cancel in the mid month, you can’t expect refunds from Ekhatabook. After cancellation of Service, you will not be charged afterwards.
                    Ekhatabook has rights to,</p>
                    <p>•	Change /Discontinue/Temporarily/Permanently stop the service.</p>
                    <p>•	Refuse or suspend or terminate your account.</p>
                    <h6>7. Copyright and Ownership</h6>
                    <p>Ekhatabook and its successor have its own intellectual property right to all protect-able components but to a limited. You may not copy, alter, reproduce, distribute any aspects of Ekhatabook Service. Ekhatabook claims no intellectual property rights over the Content you upload or provide to the Service. However, by using the Service to send Content, you agree that others may view and share your Content. Using our service, doesn’t gives you the ownership on the intellectual property on the content or the services. </p>
                   <h6> 8. Subscription to Beta Service </h6>
                    <p>Ekhatabook may sometimes offer closed or opened “Beta Services” for testing and evaluating purpose. We will only judge the testing and evaluation. We have all rights to partially discontinue or can hold temporarily or permanently at any time. By using these service, you agree that, Ekhatabook is not reliable for any modification or discontinuations.
                    9. Modification of Terms of Service
                    Ekhatabook may modify its terms at any time. Ekhatabook may, without prior notice, 
                    change the Services or the Ekhatabook Products; add or remove functionalities or 
                    features; stop providing the Services or the Ekhatabook Products, or features of 
                    the Services to the Users (specifically and generally); or create usage limits for 
                    the Services or the Products.We will intimate you by prior emails (email address associated
                     with your account)or will announce you. Also we will set effective changing date in our
                      terms of service page, date on which the new Terms of service is going to execute. We 
                      encourage your regular visit to our Terms of service page, for your reference.Following 
                      the posting of any changes in the Terms of Service, your continued use of or access 
                      to the Ekhatabook Services constitutes acceptance of such revised Terms of Service.</p>
                    <h6>10. Using API</h6>
                    <p>Accessing to these API either directly or indirectly, you are also bound to Ekhatabook’s Terms of Services. You must agree that Ekhatabook is not reliable for your data and information. Ekhatabook can suspend your account either temporarily or permanently, for frequent or unwanted API requests.</p>
                    <h6>11. General Conditions</h6>
                    <p>Your use of the service, including any content , information or functions contained within it, is provided as it is and will be available as it is without any representation or warranties. You should bear the total responsibility on risk as well as your use of our service.
                    You admit, not to store, host, duplicate, reproduce or use any fragment of the service without having written permission from Ekhatabook. You must not lease, rent, sell,transfer, sublicense, publish, modify, adapt, or translate any portion of the Software. You may not use the service to transmit any viruses, worms, or malicious content.
                    To protect integrity, confidentiality and security of your data, Ekhatabook will employ administrative, physical or technical techniques including encryption of your transmission data.
                    Ekhatabook won’t have any guarantees regarding,</p>
                    <p>•	Your ability or satisfaction to use the service.</p>
                    <p>•	Error-free availability of the service all times without interruption.</p>
                    <p>•	Mathematical accuracy of the service.</p>
                    <p>•	The correction of the bugs or errors in the service.</p>
                    <p>Ekhatabook, its affiliates and its sponsors are neither responsible nor liable for any direct, indirect, incidental, consequential, special, exemplary, punitive or other damages arising out of or relating in any way to your use of the Service. The one and only remedy for dissatisfaction with the Service is to stop using the Service.
                    The Terms of Service sets forth the entire understanding between you and Ekhatabook as to the Service and replaces any prior agreements between you and Ekhatabook (including, but not limited to, prior versions of the Terms of Service).
                    <h6>12. Third Party Links</h6>
                    <p>There may be links to third-party websites, services, offers, any events or activities in our service, that are not controlled by us. We remind you that, our terms of service and privacy policy doesn’t apply for third-party websites. We are not the assurance or responsibility to such websites, their information, materials or products. Accessing third-parties information, or providing your information to such sites are at your own risks. You agree that we are not responsible for any loss or damage of any sort in your dealings with such advertisers.</p>
                    <h6>13. Limitation of Liability</h6>
                    <p>To the fullest extent permitted by law, you assume full responsibility for and we disclaim liability to you for any indirect, consequential, exemplary, incidental, or punitive damages, including lost profits, even if we had been advised of the possibility of such damages. Ekhatabook will not be responsible for lost profits, financial data, lost data or any damages. All these things are not reasonable foreseeable.</p>
                    <h6>14.Cases of exclusion from Liability</h6>
                    <p>Liability of Output Books is excluded from the following cases which may occur due to frauds or other persons
                    <p>•	Delay or failure in performance out of our boundary</p>
                    <p>•	Losses</p>
                    <p>•	Damage</p>
                    <p>•	Penalties </p>
                    <h6>15. Indemnification</h6>
                    <p>By using this service you agree to indemnify Ekhatabook, its employer, employees, supplier and its associated workers from and against any losses, damages, fines and expenses (including attorney’s fees and costs) arising out of or relating to any claims that you have used the Services in violation of another party’s rights, in violation of any law, in violations of any provisions of the Terms, or any other claim related to your use of the Services, except where such use is authorized by Ekhatabook.</p>
                    <h6>16. Governing Law and Jurisdiction</h6>
                    <p>The Terms of Service and the relationship between you and Ekhatabook shall be governed by the laws of the Republic of India without regard to its conflict of law provisions. You and Ekhatabook agree to submit to the personal and exclusive jurisdiction of the courts located at Ajmer, India.
                    Any questions regarding the Terms of Service should be addressed to legal@Ekhatabook.com.</p>
                
            
        </div>
    

    @include('common.footer')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>