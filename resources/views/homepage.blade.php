<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home|SOMEYA</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('web_assets/images/someyalogo.jpeg')}}">

    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>  
    @include('common.header',['products'=>$products])
    <div class="" style="margin-top: 0px;"></div>
    <div class="">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                @foreach ($banners as $key => $banner)
                    <div class="carousel-item {{($key == 0 ? 'active' : '')}}">
                        <img src="{{$banner->image}}" class="d-block w-100" alt="...">
                    </div>
                @endforeach
                {{-- <div class="carousel-item">
                    <img src="assets/images/bnner.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/images/bnner.jpg" class="d-block w-100" alt="...">
                </div> --}}
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>

  <!--==================== who we are Start Here ===================================-->
  <section class="who-we-are-sc mt-5 mb-5">
    <div class="container">
        <h4 class="landing-page-main-heading">Someya Readymade </h4>
        <p class="landing-page-sub-heading">Who We Are</p>

        <div class="row mt-4">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <img src="{{asset('assets/images/one.jpg')}}" width="100%" style="border-radius: 10px;" class="mb-3">

            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
                <p style="font-size: 14px;"><b> Someya Readymade </b> is a <b>leading manufacturer and supplier of
                        uniforms </b>across industries including
                    Education, Corporate, Industrial, Healthcare, and Hospitality. It is the latest brand launched
                    by
                    Sona Group – Sona Printers LLP, a leader in Printing and Paper Industry since 1977. Since then,
                    Sona
                    Group has diversified into various verticals, ensuring high-end quality which is synonymous with
                    “Sona”. Our company – Sona Signature has positioned itself as a dominant player in the uniform
                    manufacturing industry, with its superlative quality of fabrics and stitching. We’re a company
                    founded and sustained on the principles of Trust, Support, and Mutual Respect. A crisp uniform
                    speaks volumes about your organization and if Quality, Presentation, and Customer-Centric
                    Service
                    are important to you, then look no further as Sona Signature Uniform has got you covered.</p>
                <a href="#" style="color: #ffaa46;">Know More</a>
            </div>
        </div>
    </div>
</section>
<!--==================== who we are end Here ===================================-->

<!--==================== Product Start Here ===================================-->
<section class="product-sc">
    <div class="container">
        <h4 class="landing-page-main-heading">OUR MOST SELLING </h4>
        <p class="landing-page-sub-heading">Uniform Category</p>

        <div class="row mt-5">
            @foreach ($products as $product)
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <div class="prduct-card">
                        <img src="{{$product->image}}" class="img-fluid mb-2" width="100%">
                        <div class="product-card-sc">
                            <p class="prduct-card-heading">{{$product->name}}</p>
                            <p class="prduct-card-sub-heading">{{ substr($product->content, 0, 100) }}..</p>
                            <a href="{{url('cms-product/view/' . $product->product_id)}}" class="read-more">Read More</a>
                        </div>
                    </div>
                </div>
            @endforeach    
        </div>
    </div>
</section>
<!--==================== Product  End Here ===================================-->

<!--==================== who we are Start Here ===================================-->
<section class="who-we-are-sc mt-5" id="contact-us-section">
    <div class="container">
        <h4 class="landing-page-main-heading">HAVE YOU GOT A UNIFORM VISION?
        </h4>
        <p class="landing-page-sub-heading">Looking for a new supplier you can trust? If you are looking for
            quality, <br> durability, and top-notch designs, look no further.
            Get in touch with us today!
        </p>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5">
                <div class="text-center">
                    <img src="{{asset('assets/images/mail.png')}}" width="65%" class="img-fluid">
                </div>
            </div>

            <div class="col-lg-7 col-md-7 col-sm-7" style="padding: 20px 45px;
            background-color: #fff3cd;
            border-radius: 10px;">
                <form class="row g-2" action="{{url('submit-contact-us')}}" method="POST">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="form-label"> Full Name</label>
                            <input type="text"  name="full_name" id="full_name" class="form-control form-control-custom" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="form-label"> Phone</label>
                            <input type="text" name="phone_no" id="phone_no" class="form-control form-control-custom" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="form-label"> City</label>
                            <input type="text" name="city" id="city" class="form-control form-control-custom" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="form-label"> Category</label>
                        <select class="form-select" name="category_id" id="category_id" style="height: 39px;border-radius: 10px;" required>
                            <option value="">Select Category</option>
                            @foreach ($products as $product)
                                    <option value="{{$product->product_id}}">{{$product->name}}</option>
                                @endforeach
                        </select>
                    </div>

                    
                    <div class="col-12">
                        <label for="exampleFormControlTextarea1" class="form-label">Pieces</label>
                        <input type="text" name="message" id="message" class="form-control form-control-custom" required>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn custom-btn btn-customs">Submit</button>
                    </div>
                </form>
            </div>
        </div>


    </div>
</section>
<!--==================== who we are end Here ===================================-->

<!--==================== who we are Start Here ===================================-->
<section class="who-we-are-sc mt-5">
    <div class="container">
        <h4 class="landing-page-main-heading">Our Custom</h4>
        <p class="landing-page-sub-heading">Uniform Manufacturing Expertise</p>

        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="our-custom-card">
                    <img src="{{asset('assets/images/target.png')}}" width="40%">
                    <h5>Personalized Designs</h5>
                    <p>We blend our creativity and your requirements to bring fresh and comfortable uniforms.</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="our-custom-card">
                    <img src="{{asset('assets/images/competitive.png')}}" width="40%">
                    <h5>Competitive Pricing </h5>
                    <p>Our objective is to provide quality uniforms, focussing on value based pricing.</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="our-custom-card">
                    <img src="{{asset('assets/images/material-management.png')}}" width="40%">
                    <h5>Quality Material</h5>
                    <p>Our offered uniforms are not just comfortable but highly durable to last for years.</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="our-custom-card">
                    <img src="{{asset('assets/images/real-time.png')}}" width="40%">
                    <h5>On Time Delivery</h5>
                    <p>We make sure that we deliver the goods at the promised time, assuring no delays.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--==================== who we are end Here ===================================-->
@include('common.footer',['products'=>$products,'links'=>$links])

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>

 


{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>





</body>

</html> --}}