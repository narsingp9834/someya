@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- jQuery Validation -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="edit-profile" method="POST" action="{{url('profile/update')}}" autocomplete="off" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.edit")}} {{__("labels.profile.profile")}}</h5>
                        </div>
                        <div class="card-body">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <input type="hidden" name="seller_id" value="{{$user->getSeller->seller_id}}">
                            <div class="row">        
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label required" for="company_name">Firm Name</label>
                                            <input type="text" class="form-control" id="user_fname" name="user_fname" placeholder="Firm Name" value="{{$user->user_fname}}" maxlength="50" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label required" for="user_phone_no">GST No</label>
                                            <input type="text" class="form-control" name="gst_no" id="gst_no" placeholder="GST No." value="{{$user->getSeller->gst_no}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label required" for="user_phone_no">{{__("labels.phone")}}</label>
                                            <input type="text" class="form-control" name="user_phone_no" id="user_phone_no" placeholder="Phone No." value="{{$user->user_phone_no}}" required>
                                        </div>
                                    </div>
                               
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label id="for" class="">State</label>
                                            <select class="form-control" id="state_id" name="state_id" class="form-control" required> 
                                                <option value="">Select State</option>
                                                @foreach ($states as $state)
                                                    <option value="{{$state->state_id}}" {{($user->getSeller->state_id == $state->state_id) ? 'selected' : ''}}>{{$state->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
    
                                    </div>
                                    <input type="hidden" id="selected_city" value="{{$user->getSeller->city_id}}">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label id="for" class="">City</label>
                                            <select class="form-control" id="city_id" name="city_id" class="form-control" required> 
                                                <option value="">Select City</option>
                                            </select>
                                        </div>
    
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="for" class="">Address</label>
                                           <textarea class="form-control" name="address" id="address">{{$user->getSeller->address}}</textarea>
                                        </div>
    
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="">Scanner Link</label>
                                            <input type="text" class="form-control" name="scanner_link" id="scanner_link" value="{{$user->getSeller->scanner_link}}">
                                        </div>
    
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="">Profile Text</label>
                                            <textarea class="form-control" name="description" id="description">{{$user->getSeller->description}}</textarea>
                                        </div>
    
                                    </div>

                                    @if(Auth::user()->user_type == 'B')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="">Plan Duration</label>
                                            <textarea class="form-control" id="description">{{$user->duration}}</textarea>
                                        </div>
    
                                    </div>
                                    @endif

                               
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button>
                                        <a href="{{url('/home')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
<script src="{{ asset('js/additional-methods.js') }}"></script>
    <script src="{{ asset('js/profile.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection

