<!--=========================== ===============================-->
    
<html>

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Product|SOMEYA</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('web_assets/images/someyalogo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
    .card-section{
    margin-top: 70px;
    
}
.card-section-sc{
   background-color: #fff;
   padding: 25px 25px;
   border-radius: 10px;
    
}
.card-left-sc{
    padding: 10px 25px;
    
}
.card-right-sc{
    border-radius: 25px;
    padding: 10px 25px;
}
.img-right{
    border-radius: 25px;
}
.card-section-heading{
    margin-top: 5rem;
    margin-bottom: 25px;
    border-bottom: 2px solid #1967d2;
}
.card-left-sc h6{
   padding: 5px 10px;
   border-radius: 5px;
    background-color: #1967d2;
    color: #fff;
    font-size: 14px;
    width: fit-content;
    margin-bottom: 25px;
}
.card-left-sc p{
    color: #202124;
    font-size: 15px;
}

</style>

<body>
    
    <!---======================Header Start Here =====================================-->
    @include('common.header',['products'=>$products])
    <!---======================Header End Here =====================================-->

<div class="card-section mt-5">
        

    <div class="container">
        
        <div class="row card-section-sc">
            <p>{{strip_tags($link->content)}}</p>
                       

        </div>
    </div>
</div>
    <!--=========================== Footer Start Here ===============================-->
    @include('common.footer',['products'=>$products, 'links'=>$links])
    <!---======================Footer End Here =====================================-->


    <!---======================Sub Footer Start Here =====================================-->
    <section class="sub-footer-sc">
        <p>© 2024 data . All Right Reserved.</p>
    </section>
    <!---======================Sub Footer End Here =====================================-->
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>