<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home|SOMEYA</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('web_assets/images/someyalogo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<style>
@media only screen and (max-width: 576px) {
    .navbar-custom .nav-link {
   
        color: #b7b7b7;
    border-bottom: 1px solid #5e5e5e;
   
}
.custom-btn{

    margin-top:15px;
    background-color: #ff4343;
    border-radius: 5px;
    padding: 7px 15px;
    font-size: 15px;
    color: #fff;
    margin-right: 5px;
    font-size:13px;

}
  }

  .close-btnn{
    font-size: 21px;
    color:#b7b7b7;
  }

  a{
    text-decoration: none;
    
  }
  .heading-main a{
    color:#ff4343;
    border:2px dotted #ff4343;
    padding:5px 12px;
    margin-bottom:25px

  }

  .tab-cousres-card .span-badge {
    background-color: #ffaa46 !important;
    color: #000000 !important;
    border-radius:5px;
    
}

.cousres-banner-card {
    background-color: #ff434314;
    border-radius: 25px;
    padding: 50px 45px;
    width: 100%;
    max-width: 950px;
    margin: auto;
    margin-bottom: 5rem;
    margin-top: 2rem;
    border-bottom: 3px solid #ff4343;
}

.btn-custom-ss {
    background-color:#ff4343;
    color: #fff;
    font-size: 16px;
    padding: 9px 20px;
    border-radius: 4px;
    font-family: 'Poppins', sans-serif !important;
    transition: all .5s ease;
    box-shadow: 0 0.5rem 1.125rem -0.5rem rgba(162, 105, 254, 0.9);
}
</style>

<body>
    @include('common.header',['products'=>$products])
     
    <!-- Slider Start here -->
    <section class="all-section mb-5">
        <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">

                @foreach ($banners as $key => $banner)
                    <div class="carousel-item {{($key == 0 ? 'active' : '')}}">
                        <img src="{{$banner->image}}" class="d-block w-100" alt="...">
                    </div>
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- Slider End Here -->

    <!-- Coding brand logo start here  -->

    <div class="brand-logo-slider-sc">
        <div class="container">
            <h2 class="text-center mb-5">Featured In </h2>

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-4">
                    <div class="featured-logo-img">
                        <img src="" class="img-fluid">
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- Coding brand logo end here -->

    <!-- all courses start here -->
    <section class="courses-section">
        <div class="container">
            <div class="courses-heading-sc">
                <h2 class="cousres-main-heading text-center">OUR MOST SELLING <span
                        class="common-heading-color">TRAINING</span>
                </h2>
                <p class="courses-sub-heading text-center" style="border-bottom: 3px solid #ffaa46">UNIFORM CATEGORIES
                </p>
            </div>

            <div class="row">
                @foreach ($products as $product)
                    <div class="col-lg-3 col-md-4 col-sm-3 col-12">
                        <div class="courses-card" style="height:350px;padding-bottom:15px">
                            <div class="courses-img-sc">
                                <img src="{{$product->image}}" class="img-fluid courses-img" width="100%">
                            </div>
                            <div class="courses-text-sc text-center">
                                <h5>{{$product->name}}
                                </h5>
                                <p class="cousres-sub-heading">{{ substr($product->content, 0, 100) }}.</p>

                                <a href="{{url('cms-product/view/' . $product->product_id)}}" class="btn btn-courses-btn" style="color:red">READ MORE</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>



    
            
                
        </div>
    </section>



    <section class="all-section mb-5">
        <div id="carouselExampleAutoplaying2" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">

                @foreach ($customerBanners as $key => $customerBanner )
                    <div class="carousel-item {{($key == 0 ? 'active' : '')}}">
                        <img src="{{$customerBanner->image}}" class="d-block w-100" alt="...">
                    </div>
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying2"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying2"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>


    <!-- all courses end here -->

    <!--- start  -->
    <div class="container">
        <div class="cousres-banner-card">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="cousres-banner-card-left">
                        <h2>Let’s find the right <br> Stitching Platform</h2>
                        <p>Customer satisfaction relies more on stitching quality, and our talented skilled tailors take care of the minute details that make our uniforms unique.<br> your goals are closer than
                            ever!</p>
                        <a href="#" class="btn btn-custom">Start with us</a><br>
                    </div>
                </div>

                <div class="col-lg- col-md-5 col-sm-5">
                    <div class="cousres-banner-card-left">
                        <img src="{{asset('web_assets/images/industrialuniform.jpeg')}}" class="img-fluid" style="border-radius: 25px; width:350px">
                    </div>
                </div>

            </div>



        </div>

        <h1 class="text-center">How Can We Help?</h1>
        <form action="{{url('submit-contact-us')}}" method="POST" id="contat-us-form">
            @csrf
            <div class="row my-4" id="contact-us">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label id="">Name</label>
                            <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Your Name" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label id="">Email</label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="Your Email" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label id="">Phone</label>
                            <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="Your Phone" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label id="">Category</label>
                        <select name="category_id" id="category_id" class="form-control" required>
                                <option value="">Select Category</option>
                                @foreach ($products as $product)
                                    <option value="{{$product->product_id}}">{{$product->name}}</option>
                                @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label id="">Message</label>
                        <textarea class="form-control" name="message" placeholder="Your Message" required></textarea>
                        </div>
                    </div>

                    <div class="col-md-12 mt-2 text-center">
                        <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
            </div>
        </form>    
    </div>
    <!--- end  -->


    @include('common.footer',['products'=>$products,'links'=>$links])

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>