<div class="modal fade" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="contactUsModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-advisor" role="document">
        <div class="modal-content">
            <div>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center">How Can We Help?</h1>
                <form action="{{url('submit-contact-us')}}" method="POST" id="contat-us-form">
                    @csrf
                    <div class="row my-4" id="contact-us">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label id="">Name</label>
                                    <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Your Name" required> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label id="">Email</label>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Your Email" required> 
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="form-group">
                                    <label id="">Phone</label>
                                    <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="Your Phone" required> 
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="form-group">
                                    <label id="">Category</label>
                                <select name="category_id" id="category_id" class="form-control" required>
                                        <option value="">Select Category</option>
                                        @foreach ($products as $product)
                                            <option value="{{$product->product_id}}">{{$product->name}}</option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-12 mt-2">
                                <div class="form-group">
                                    <label id="">Message</label>
                                <textarea class="form-control" name="message" placeholder="Your Message" required></textarea>
                                </div>
                            </div>
        
                            <div class="col-md-12 mt-2 text-center">
                                <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                    </div>
                </form>   
            </div>
        </div>
    </div>
</div>


 <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">How Can We Help?</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i
                        class="bi bi-x-lg" data-bs-dismiss="modal" aria-label="Close"
                        style="cursor: pointer;background-color: #ffaa46;padding: 6px 10px;border-radius: 50%;color: #fff;"></i></button>
            </div>
            <div class="modal-body">

                <div class="form-model">
                    <form action="{{url('submit-contact-us')}}" method="POST">
                        @csrf
                        <div class="form-group mb-2">
                            <label class="form-label"> Full Name</label>
                            <input type="text" name="full_name" id="full_name"  class="form-control form-control-custom">
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label"> Phone</label>
                            <input type="text" name="phone_no" id="phone_no" class="form-control form-control-custom">
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label"> City</label>
                            <input type="text" name="city" id="city" class="form-control form-control-custom" required>
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label"> Category</label>
                            <select name="category_id" id="category_id" class="form-select" style="height: 39px;border-radius: 10px;" required>
                                <option value="">Select Category</option>
                                @foreach ($products as $product)
                                    <option value="{{$product->product_id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1" class="form-label">Message</label>
                            <input type="text" name="message" id="message" class="form-control form-control-custom" required>
                        </div>

                        <button type="submit" class="btn custom-btn btn-customs">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <footer class="footer-sc">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="footer-img-sc">
                    <img src="{{asset('web_assets/images/someyalogo.jpeg')}}" class="img-fluid" width="30%">
                    <p>We are a uniform manufacturer, uniform wholesaler and uniform supplier 
                        of superlative quality uniforms in all sectors.</p>
                    <br>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="footer-link-sc">
                    <h6 class="footer-link-heading">Quick Links </h6>
                    <ul>
                        @foreach ($links as $link)
                         <li><a href="{{url('quick-link/view/'.$link->link_id)}}">{{$link->name}}</i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="footer-link-sc">
                    <h6>Customer Service </h6>
                    <ul>
                        @foreach ($products as $product)
                            <li><a href="{{url('cms-product/view/' . $product->product_id)}}">{{$product->name}}</i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="footer-link-sc">
                    <h6>CONTACT US </h6>
                    <ul>
                        <li><a href="#">Office No: 8005710439 </i></a></li>
                        <li><a href="#"> Support No: 8005710439 </i></a></li>
                        <li><a href="#">Address: All Send School ,
                                </i></a></li>
                        <li><a href="#">Dhola Bhata Ajmer ( 305001)</i></a></li>
                        <li><a href="#">Rajasthan</i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer> --}}

<!--==================== Footer Start Here ===================================-->
<section class="footer-section mt-5">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-logo-sc" style="padding: 0px 17px;">
                        <img src="{{asset('assets/images/someyalogo.jpeg')}}" width="70px" style="border-radius: 50%;"
                            class="mb-3"></a>
                    </div>
                    <p style="color: #FFFFFFA1;font-size: 14px;">We are a uniform manufacturer, uniform wholesaler
                        and uniform supplier of superlative quality uniforms and workwear, providing uniform to
                        diverse range of industries like School Dress
                        sectors.
                    </p>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-link-sc">
                        <h5 class="footer-link-main-heading">Quick Links </h5>
                        <ul>
                            @foreach ($links as $link)
                                <li><a href="{{url('quick-link/view/'.$link->link_id)}}">{{$link->name}}</i></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-link-sc">
                        <h5 class="footer-link-main-heading">Customer Service  </h5>
                        <ul>
                            @foreach ($products as $product)
                                <li><a href="{{url('cms-product/view/' . $product->product_id)}}">{{$product->name}}</i></a></li>
                             @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-link-sc">
                        <h5 class="footer-link-main-heading">CONTACT US </h5>
                        <ul>
                            <li><a href="#">Office No: 8005710439 </i></a></li>
                            <li><a href="#"> Support No: 8005710439 </i></a></li>
                            <li><a href="#">Address: All Send School ,
                                    </i></a></li>
                            <li><a href="#">Dhola Bhata Ajmer ( 305001)</i></a></li>
                            <li><a href="#">Rajasthan</i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer-line"></div>
                <span class="reserved-text">© 2023 Sona Signature LLP. All rights reserved.</span>
            </div>
        </div>
    </footer>
</section>
<!--==================== Footer End Here ===================================-->

{{-- <div class="sub-footer">
    <div class="container">
        <div class="text-center">
            <p>TERMS & CONDITIONS RESERVED - SOMEYAREADYMADE | 2023-24</p>
        </div>
    </div>
</div> --}}