
<div class="sub-nav fixed-top-custom">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="my-1">
                    <p>Talk to Uniform Consultant call 8005710439 Mon - Sat (10:30 AM to 7:30 PM) </p>
                </div>
            </div>

            <!--<div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="sub-nav-right">
                        <p><i class="bi bi-telephone"></i> +91 6350560489</p>
                    </div>
                </div>-->
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg fixed-top navbar-custom">
    <div class="container">
        <a class="navbar-brand" href="index.php"><img src="{{asset('web_assets/images/someyalogo.jpeg')}}" class="img-fluid" width="60px"></a>
      
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
                
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-center flex-grow-1 pe-3">
                    <li class="nav-item">
                        <a class="nav-link nav-link-custom" href="{{url('/')}}">Home</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link nav-link-custom" href="{{url('/')}}">About Us</a>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Our Products
                        </a>
                        <ul class="dropdown-menu">
                            @foreach ($products as $product)
                                <li><a class="dropdown-item" href="{{url('cms-product/view/' . $product->product_id)}}">{{$product->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link nav-link-custom" href="{{url('contact-us')}}">Contact Us</a>
                    </li>
                </ul>
                <div class="d-flex">
                    <button class="btn btn-custom" data-bs-toggle="modal" data-bs-target="#contactUsModal">ENQUIRE NOW</a>
                </div>
            </div>
        </div>
    </div>
</nav>