<div class="sub-menu">
    <div class="container">
        <p>Talk to Uniform Consultant call 8005710439 Mon - Sat (10:30 AM to 7:30 PM)</p>
    </div>
</div>
<!--==================== Header Start Here ===================================-->
<nav class="navbar navbar-expand-lg navbar-custom">
    <div class="container">
        <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/images/someyalogo.jpeg')}}" width="60px"
                style="border-radius: 50%;"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar"
            aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
            <i class="bi bi-menu-app"></i>
        </button>
        <div class="offcanvas offcanvas-start offcanvas-custom " tabindex="-1" id="offcanvasNavbar"
            aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasNavbarLabel"><span class="menu-heading">Menu</span></h5>

                <p class="btn-close icon-close-custom" data-bs-dismiss="offcanvas" aria-label="Close">Close</p>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-center flex-grow-1 pe-3 mx-2">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">Home</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Our Products
                        </a>
                        <ul class="dropdown-menu">
                            @foreach ($products as $product)
                                <li><a class="dropdown-item" href="{{url('cms-product/view/' . $product->product_id)}}">{{$product->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('our-quality')}}">Our Quality</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('why-us')}}">Why Us</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">About Us</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
                    </li>
                </ul>
                <form class="">
                    <a href="#" class="btn custom-btn" data-bs-toggle="modal" data-bs-target="#exampleModal">Book
                        Now</a>
                </form>
            </div>
        </div>
    </div>
</nav>

{{-- <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">How Can We Help?</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i
                        class="bi bi-x-lg" data-bs-dismiss="modal" aria-label="Close"
                        style="cursor: pointer;background-color: #ffaa46;padding: 6px 10px;border-radius: 50%;color: #fff;"></i></button>
            </div>
            <div class="modal-body">

                <div class="form-model">
                    <form action="{{url('submit-contact-us')}}" method="POST">
                        @csrf
                        <div class="form-group mb-2">
                            <label class="form-label"> Full Name</label>
                            <input type="text" name="full_name" id="full_name"  class="form-control form-control-custom">
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label">Email ID</label>
                            <input type="email"  name="email" id="email" class="form-control form-control-custom">
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label"> Phone</label>
                            <input type="text" name="phone_no" id="phone_no" class="form-control form-control-custom">
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label"> Category</label>
                            <select name="category_id" id="category_id" class="form-select" style="height: 39px;border-radius: 10px;" required>
                                <option value="">Select Category</option>
                                @foreach ($products as $product)
                                    <option value="{{$product->product_id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1" class="form-label">Message</label>
                            <textarea class="form-control form-control-customs" rows="3" name="message" required></textarea>
                        </div>

                        <button type="submit" class="btn custom-btn btn-customs">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}