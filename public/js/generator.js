
// Datatable
$(document).ready(function () {
    if(document.getElementById('switchCount')){
        var count = $('#switchCount').val();
        for (let index = 0; index < count; index++) {
            initializeFlatpickr(index);
        }
    }else{
        initializeFlatpickr(0);
    }
   
    //Start Datatable
    var table;
    if (document.getElementById("user_type")) {
        var user_type = $('#user_type').val();
    }
    if (document.getElementById("table_generator")) {
        table = $('#table_generator').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/generator/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status   = jQuery("#status").val();
                    data.zone     = jQuery("#zone").val();
                    data.state    = jQuery("#state_id").val();
                    data.city     = jQuery("#city_id").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'generator_name', name: 'generators.generator_name' }, //1
                { data: 'model_number', name: 'generators.model_number'},
                { data: 'serial_number', name: 'generators.serial_number'},
                { data: 'user_name', name: 'user_name'}, // 4
                { data: 'email', name: 'users.email'}, // 5
                { data: 'zip_code', name: 'customer_addresses.zip_code'},
                { data: 'zone', name: 'zones.zone_name'},
                { data: 'city', name: 'cities.name'},
                { data: 'state', name: 'states.name'},
                { data: 'generator_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });
        // Hide/Show columns
        if(user_type == 'C'){
            var columnsToHide = [4, 5];    
            table.columns(columnsToHide).visible( ! table.columns(columnsToHide).visible() );
        } 
        $('#status').on('change', function () {
            table.draw();
        })
    
        $('#zone').on('change', function () {
            table.draw();
        })
    
        jQuery("#state_id").on("change", function () {
            table.draw();
        });
    
        jQuery("#city_id").on("change", function () {
            table.draw();
        });
    }
    //End Datatable

    // Fetch customer list based on customer_type
    $('#customer_type').on('change', function(){
        getCustomer();
    })

    if ($("#customer_type").val() != '' && $("#customer_type").val() != undefined) {
        getCustomer();
    }
   
    // Fetch customer address & types and append
    $('#customer_id').on('change', function(){
        $("#loader").show();
        var user_type = $("#user_type").val();
        var customer_id = $("#customer_id").val();
        var service_id = $("#service_id").val();
        var dservice_id = $("#dservice_id").val();
        if(user_type == "SA" || user_type == "A"){
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: base_url+'/customer/generators/'+customer_id,
                success: function(data) {
                    $("#loader").hide();
                    if(data.status == "success"){
                        getUser();
                        if(dservice_id == 4){
                            window.location.href=base_url+'/appointment/choose-question/'+service_id+'/'+customer_id;
                        }else{
                            if(data.is_empty == "Yes"){
                                $(".gcard").css('display','block');
                            }else{
                                window.location.href=base_url+'/appointment/choose-generator/'+service_id+'/'+customer_id;
                                $(".gcard").css('display','none');
                            }
                        }
                    }
                },
                error: function(error) {
                    toastr.error('Something went wrong');
                },
            });
        }else{
            getUser();
        }
    });

    if ($("#customer_id").val() != '' && $("#customer_id").val() != undefined) {
        getUser($("#customer_id").val());
    }
    
    $('#address_type_id').on('change', function(){
        var address_type_id = $(this).val();
        getAddressDetails(address_type_id);
    })
});

function getCustomer() {
    $('#customer_id').empty();
    $('#city_id').val('').trigger('change');
    clearField();
    clearUserField();
    if($('#customer_type').val() == "Residential"){
        $("#custDiv").show();
        $("#comDiv").hide();
    }else{
        $("#custDiv").hide();
        $("#comDiv").show();
    }

    var customer_type = $('#customer_type').val();
    var token = jQuery("input[name='_token']").val();
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: base_url+'/generator/get-customer',
        data: {
            _token : token,
            customer_type     : customer_type,
        },
        success: function(data) {
            // success
            var status = data.status;
            var msg = data.msg;
            
            if(status == 'success'){
                var details = data.details;
                if ($("#customer_id").val() == '') {
                clearField();
                clearUserField();
                }
                if(details != null){
                    $('#customer_id').append(`<option value=""> Select Customer </option>`);                            
                    for (let index = 0; index < details.length; index++) {    
                        var oldCustomer_id = $('#oldCustomer_id').val();  
                        var viewCustomerId = $('#viewCustomerId').val();    
                        if(oldCustomer_id == details[index]['customer_id'] || viewCustomerId == details[index]['customer_id']) {
                            var selected = 'selected';
                        }else{
                            var selected = '';
                        }                
                            
                        $('#customer_id').append(`<option value="${details[index]['customer_id']}" ${selected}> ${details[index]['name']} </option>`);                            
                    }
                }                    
            }else{
                toastr.error(msg);
            }
        },
        // error: function(error) {
        //     // error
        //     toastr.error('Something went wrong');
        // },
    });
}

function clearUserField(){
    // $('#customer_id').empty();
    // if ($("#customer_type").val() != '') {
    //     $("#customer_type").trigger('change');
    // }

    $('#user_fname').val(null);
    $('#user_lname').val(null);
    $('#company_name').val(null);
    $('#company_contact_name').val(null);

    $('#email').val(null);
    $('#user_country_code').val(null);
    $('#user_phone_no').val(null);
    // $('#customer_category').val(null);
}

// Clear Address fields
function clearField(){
    $('#address_type_id').empty();
    $('#address_type_id').append(`<option value=""> Select Address Type </option>`);

    $('#country_id').val(null);
    $('#state_id').val(null);
    $('#city_id').val(null).trigger('change');

    $('#address_line_1').text(null);
    $('#address_line_2').text(null);
    $('#zip_code').val(null);
    $('#description').text(null);
}

// Fill Address fields
function fillField(country, state, city, address_line_1, address_line_2, zip_code, description){
    $('#country_id').val(country);
    $('#state_id').val(state).trigger('change');
    $('#address_line_1').text(address_line_1);
    $('#address_line_2').text(address_line_2);
    $('#zip_code').val(zip_code);
    $('#description').text(description); 
}

// get address bade on type (multiple address_type)
function getAddressDetails(address_type_id){
    var token = jQuery("input[name='_token']").val();
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: base_url+'/common/get-address',
        data: {
            _token : token,
            address_type_id     : address_type_id,
            customer_id :   $('#customer_id').val(),
        },
        success: function(data) {
            var status  = data.status;
            var msg     = data.msg;
            if(status == 'success'){ 
                var Address = data.Address;

                var country = Address['country'];
                var state = Address['state'];
                var city = Address['city'];
                var address_line_1 = Address['address_line_1'];
                var address_line_2 = Address['address_line_2'];
                var zip_code = Address['zip_code'];
                var description = Address['description'];

                $("#selected_city").val(city);
                fillField(country, state, city, address_line_1, address_line_2, zip_code, description);
            }else{
                // toastr.error(msg);
                $('#country_id').prop('disabled', false);
                $('#state_id').prop('disabled', false);
                $('#city_id').prop('disabled', false);
                $('#address_line_1').prop('readonly', false);
                $('#address_line_2').prop('readonly', false);
                $('#zip_code').prop('readonly', false);
                $('#description').prop('readonly', false);
            }
        },
        error: function(error) {
            toastr.error('Record not found');
        },
    });
}

function getUser(customer_id = null) {
    var checkCustomer_id = customer_id;
    var address_type_id = $('#oldCustomerAddressId').val();
    if(customer_id != null) {
        var customer_id = customer_id;
    }else{
        var customer_id = $('#customer_id').val();
    }
    var token = jQuery("input[name='_token']").val();

    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: base_url+'/generator/get-user',
        data: {
            _token : token,
            customer_id     : customer_id,
            address_type_id : address_type_id,
        },
        success: function(data) {
            // success
            $("#loader").hide();
            var status = data.status;
            var msg = data.msg;
            
            if(status == 'success'){
                var customer_type           = $('#customer_type').val();
                var user                    = data.user;
                var customer_categories     = data.customer_categories;
                // var category                = data.category;
                var customer_id = data.customer_id;
                var getCustomerAddressType  = data.getCustomerAddressType;
                var Address                 = data.Address;

                if(customer_type == 'Residential'){
                    $('#user_fname').val(user['user_fname']);
                    $('#user_lname').val(user['user_lname']);
                }else if(customer_type == 'Commercial'){
                    $('#company_name').val(customer_categories['company_name']);
                    $('#company_contact_name').val(customer_categories['company_contact_name']);
                }

                $('#email').val(user['email']);
                $('#user_country_code').val(user['user_country_code']);
                $('#user_phone_no').val(user['user_phone_no']);
                // $('#customer_category').val(category['category_name']);
                $('#customer_id_selected').val(customer_id);
                if(checkCustomer_id == '') {
                    clearField();
                }
                if( getCustomerAddressType != null ){
                    if(getCustomerAddressType.length == 1) {
                        $('#address_type_id').empty();
                        if($('#oldCustomerAddressId').val() == getCustomerAddressType[0]['address_type_id']) {
                            var selected = 'selected';
                        }else{
                            var selected = '';
                        }
                        $('#address_type_id').append(`<option value="${getCustomerAddressType[0]['address_type_id']}" ${selected}> ${getCustomerAddressType[0]['address_type_name']} </option>`);
                        if(Object.keys(Address).length > 0){
                            var country = Address['country'];
                            var state = Address['state'];
                            var city = Address['city'];
                            var address_line_1 = Address['address_line_1'];
                            var address_line_2 = Address['address_line_2'];
                            var zip_code = Address['zip_code'];
                            var description = Address['description'];

                            $('#country_id').attr('disabled', true);
                            $('#state_id').attr('disabled', true);
                            $('#city_id').attr('disabled', true);
                            $('#address_line_1').attr('readonly', true);
                            $('#address_line_2').attr('readonly', true);
                            $('#zip_code').attr('readonly', true);
                            $('#description').attr('readonly', true);

                            $("#selected_state").val(state);
                            $("#selected_city").val(city);
                            fillField(country, state, city, address_line_1, address_line_2, zip_code, description)                            
                        }else{
                            $('#country_id').attr('disabled', false);
                            $('#state_id').attr('disabled', false);
                            $('#city_id').attr('disabled', false);
                            $('#address_line_1').attr('readonly', false);
                            $('#address_line_2').attr('readonly', false);
                            $('#zip_code').attr('readonly', false);
                            $('#description').attr('readonly', false);
                            getCity(3959);
                        }
                    } else if(getCustomerAddressType.length > 1) {
                        if(checkCustomer_id == '') {
                           clearField();
                        }
                        if(Address.length == 0){
                            clearField();
                        } 
                        for (let index = 0; index < getCustomerAddressType.length; index++) {             
                            if($('#oldCustomerAddressId').val() == getCustomerAddressType[index]['address_type_id']) {
                                var selected = 'selected';
                            }else{
                                var selected = '';
                            }                   
                            $('#address_type_id').append(`<option value="${getCustomerAddressType[index]['address_type_id']}" ${selected}> ${getCustomerAddressType[index]['address_type_name']} </option>`);    

                            if(Object.keys(Address).length > 0){
                                var country = Address['country'];
                                var state = Address['state'];
                                var city = Address['city'];
                                var address_line_1 = Address['address_line_1'];
                                var address_line_2 = Address['address_line_2'];
                                var zip_code = Address['zip_code'];
                                var description = Address['description'];
    
                                $('#country_id').attr('disabled', true);
                                $('#state_id').attr('disabled', true);
                                $('#city_id').attr('disabled', true);
                                $('#address_line_1').attr('readonly', true);
                                $('#address_line_2').attr('readonly', true);
                                $('#zip_code').attr('readonly', true);
                                $('#description').attr('readonly', true);
    
                                $("#selected_state").val(state);
                                $("#selected_city").val(city);
                                fillField(country, state, city, address_line_1, address_line_2, zip_code, description)                            
                            }else{
                                $('#country_id').attr('disabled', false);
                                $('#state_id').attr('disabled', false);
                                $('#city_id').attr('disabled', false);
                                $('#address_line_1').attr('readonly', false);
                                $('#address_line_2').attr('readonly', false);
                                $('#zip_code').attr('readonly', false);
                                $('#description').attr('readonly', false);
                                getCity(3959);
                            }
                            
                        }
                    } 
                }
            }else{
                clearUserField();
                toastr.error(msg);
            }
        },
        error: function(error) {
            // error
            toastr.error('Something went wrong');
        },
    });
}
$('#address_type_id').on('change', function(){
    var address_type_id = $(this).val();
    getAddressDetails(address_type_id);
})

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
                'generator_name': {
                    require_field: true,
                }
            },
            messages: {
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
              }
        });
    }
});