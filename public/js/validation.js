/**************************************************************************/
jQuery.validator.addMethod("strongPassword", function (value) {
    if(/^[A-Za-z0-9\d=!\-@._*\$]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value)){
        return true;
    }else if( value.length == 0){
        return true;
    }
}, "The password must contain number,lower case letter,upper case letter");

/**************************************************************************/
jQuery.validator.addMethod("validate_email", function(value, element) {
    var urlregex = /^([a-zA-Z0-9_+\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(urlregex.test(value)){
        var str = value;
        var foundString = str.substring(str.indexOf('@') + 1);       
        var count = (foundString.match(/\./g) || []).length;
        if(parseInt(count) == 1){
            return true;
        }else{
            return false;
        }
        return true;
    }else{
        return false;
    }
}, "Please enter a valid email address.");


/**************************************************************************/
jQuery.validator.addMethod("require_field", function (value, element) {
    if (value.trim() == '') {
        return false;
    }
    return true;
}, "This field is required.");

/**************************************************************************/
jQuery.validator.addMethod("nospaces", function(value, element) { 
    if( value.length == 0){
        return true;
    } else{
    //    var trimValue = value.trim();
        if(/.*\S.*/.test(value)){
      //  if(/^(?! )[A-Za-z]*(?<! )/.test(value)){
        // if(/^\S*$/u.test(value)){
        // if(/^[^\s]([a-zA-Z0-9_.+/,'\s-])+$/.test(value)){
            return true;
        }else{
            return false;
        }
    }
       
}, "Please enter valid field details.");

/**************************************************************************/
jQuery.validator.addMethod("notEqual", function (value, element, param) {
    return this.optional(element) || value != param;
}, "This field is required.");

/**************************************************************************/
jQuery.validator.addMethod("urllink", function (element,value) {
    var urlregex = /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
    var value = $('#website').val();
    if (urlregex.test(value)) {
        var count = (value.match(/.com/g) || []).length;
        if (parseInt(count) == 1) {
            return true;
        } else {
            return false;
        }
    }else if( value.length == 0){
        return true;
    } 
    else {
        return false;
    }
}, "Please enter valid URL");

/**************************************************************************/
jQuery.validator.addMethod("validate_slug", function(value, element) {
    if (/^[A-Za-z\d-.]+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "use text-text format and no space allowed");

/**************************************************************************/

/**************************************************************************/
jQuery.validator.addMethod("validate_org_slug", function(value, element) {
    if (/^[a-z\d-.]+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "use text-text format and no space allowed");

/**************************************************************************/

jQuery.validator.addMethod("lettersOnly", function(value, element) {
    if(/^[-'_a-zA-Z0-9\s]+$/i.test(value)){
        return true;
    }else if(value.length == 0){
        return true;
    }
  }, "Please enter letters Only");

/**************************************************************************/
jQuery.validator.addMethod("numbersOnly", function(value, element) {
    if(/^[0-9.()]*$/.test(value)){
        return true;
    }else if(value.length == 0){
        return false;
    }
  }, "Please enter numbers Only");

/**************************************************************************/
  jQuery.validator.addMethod("rangeOnly", function(value, element) {
    // if(/\b([1-9])\b /i.test(value)){
    if(/^([0-9])$/i.test(value)){
        return true;
    }else if(value.length == 0){
        return true;
    }
  }, "Please enter number between 1 to 9 range only");

/**************************************************************************/
  jQuery.validator.addMethod("website_url", function(value, element) {
    if(/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/g.test(value)){

    // if(/(?:https?:\/\/)?(?:www\.)?[a-z-]+\.(?:com|org|nl|in|edu|co\.uk)(?:\.[a-z]{2,3})?/.test(value)){
        return true;
    }else if(value.length == 0){
        return true;
    }
  }, "Please enter valid website url");

/**************************************************************************/
  $.validator.addMethod("validYear", function(value, element) {
    currentYear = new Date().getFullYear();
    if((parseInt(value) >= 1970) && (parseInt(value) <= currentYear)){

    // if((parseInt(value) > 1970)){
        return true;
    }else if(value.length == 0){
        return true;
    }
}, "Please enter valid year");
/***************************************************************************/

// Valid color codes

jQuery.validator.addMethod("color_code", function(value, element) {
    var urlregex = /^#([0-9a-f]{3}){1,2}$/i;
    if(urlregex.test(value)){
        return true;
    }else{
        return false;
    }
}, "Please enter a valid color.");
/**************************************************************************/

jQuery.validator.addMethod('percentage', function (value, el) {
    var urlregex = /^((0|[1-9]\d?)(\.\d{1,2})?|100(\.00?)?)$/;
    if(urlregex.test(value)){
        return true;
    }else{
        return false;
    }
}, "Percentage can't be greater than 100%.");
/**************************************************************************/

jQuery.validator.addMethod("phoneNoValidation", function(value, element) {
    if(value == "(000)000-0000" || value == "0000000000"){
    return false;
    }
    return true;
}, "* Please enter valid phone no");