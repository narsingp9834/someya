/**
 * App Calendar
 */

/**
 * ! If both start and end dates are same Full calendar will nullify the end date value.
 * ! Full calendar will end the event on a day before at 12:00:00AM thus, event won't extend to the end date.
 * ! We are getting events from a separate file named app-calendar-events.js. You can add or remove events from there.
 **/

'use-strict';

// RTL Support
var direction = 'ltr',
  assetPath = '../../../app-assets/';
if ($('html').data('textdirection') == 'rtl') {
  direction = 'rtl';
}

if ($('body').attr('data-framework') === 'laravel') {
  assetPath = $('body').attr('data-asset-path');
}

$(document).on('click', '.fc-sidebarToggle-button', function (e) {
  $('.app-calendar-sidebar, .body-content-overlay').addClass('show');
});

$(document).on('click', '.body-content-overlay', function (e) {
  $('.app-calendar-sidebar, .body-content-overlay').removeClass('show');
});

document.addEventListener('DOMContentLoaded', function () {
    //Appointment Calendar

    var calendarApt = document.getElementById('appointment-calendar'),
    eventToUpdate,
    sidebar = $('.event-sidebar'),
    calendarsColor = {
      Business: 'primary',
      Holiday: 'success',
      Personal: 'danger',
      Family: 'warning',
      ETC: 'info'
    },
    eventForm = $('.event-form'),
    addEventBtn = $('.add-event-btn'),
    cancelBtn = $('.btn-cancel'),
    updateEventBtn = $('.update-event-btn'),
    toggleSidebarBtn = $('.btn-toggle-sidebar'),
    eventTitle = $('#title'),
    eventLabel = $('#select-label'),
    startDate = $('#start-date'),
    endDate = $('#end-date'),
    eventUrl = $('#event-url'),
    eventGuests = $('#event-guests'),
    eventLocation = $('#event-location'),
    allDaySwitch = $('.allDay-switch'),
    selectAll = $('.select-all'),
    calEventFilter = $('.calendar-events-filter'),
    filterInput = $('.input-filter'),
    btnDeleteEvent = $('.btn-delete-event'),
    calendarEditor = $('#event-description-editor');

  // --------------------------------------------
  // On add new item, clear sidebar-right field fields
  // --------------------------------------------
  $('.add-event button').on('click', function (e) {
    $('.event-sidebar').addClass('show');
    $('.sidebar-left').removeClass('show');
    $('.app-calendar .body-content-overlay').addClass('show');
  });

  // Label  select
  if (eventLabel.length) {
    function renderBullets(option) {
      if (!option.id) {
        return option.text;
      }
      var $bullet =
        "<span class='bullet bullet-" +
        $(option.element).data('label') +
        " bullet-sm mr-50'> " +
        '</span>' +
        option.text;

      return $bullet;
    }
    eventLabel.wrap('<div class="position-relative"></div>').select2({
      placeholder: 'Select value',
      dropdownParent: eventLabel.parent(),
      templateResult: renderBullets,
      templateSelection: renderBullets,
      minimumResultsForSearch: -1,
      escapeMarkup: function (es) {
        return es;
      }
    });
  }

  // Guests select
  if (eventGuests.length) {
    function renderGuestAvatar(option) {
      if (!option.id) {
        return option.text;
      }

      var $avatar =
        "<div class='d-flex flex-wrap align-items-center'>" +
        "<div class='avatar avatar-sm my-0 mr-50'>" +
        "<span class='avatar-content'>" +
        "<img src='" +
        assetPath +
        'images/avatars/' +
        $(option.element).data('avatar') +
        "' alt='avatar' />" +
        '</span>' +
        '</div>' +
        option.text +
        '</div>';

      return $avatar;
    }
    eventGuests.wrap('<div class="position-relative"></div>').select2({
      placeholder: 'Select value',
      dropdownParent: eventGuests.parent(),
      closeOnSelect: false,
      templateResult: renderGuestAvatar,
      templateSelection: renderGuestAvatar,
      escapeMarkup: function (es) {
        return es;
      }
    });
  }

  // Start date picker
  if (startDate.length) {
    var start = startDate.flatpickr({
      enableTime: true,
      altFormat: 'Y-m-dTH:i:S',
      onReady: function (selectedDates, dateStr, instance) {
        if (instance.isMobile) {
          $(instance.mobileInput).attr('step', null);
        }
      }
    });
  }

  // End date picker
  if (endDate.length) {
    var end = endDate.flatpickr({
      enableTime: true,
      altFormat: 'Y-m-dTH:i:S',
      onReady: function (selectedDates, dateStr, instance) {
        if (instance.isMobile) {
          $(instance.mobileInput).attr('step', null);
        }
      }
    });
  }

  // Event click function
  // function eventClick(info) {
  //   console.log("hii");
  //   eventToUpdate = info.event;
  //   if (eventToUpdate.url) {
  //     info.jsEvent.preventDefault();
  //     window.open(eventToUpdate.url, '_blank');
  //   }

  //   sidebar.modal('show');
  //   addEventBtn.addClass('d-none');
  //   cancelBtn.addClass('d-none');
  //   updateEventBtn.removeClass('d-none');
  //   btnDeleteEvent.removeClass('d-none');

  //   eventTitle.val(eventToUpdate.title);
  //   start.setDate(eventToUpdate.start, true, 'Y-m-d');
  //   eventToUpdate.allDay === true ? allDaySwitch.prop('checked', true) : allDaySwitch.prop('checked', false);
  //   eventToUpdate.end !== null
  //     ? end.setDate(eventToUpdate.end, true, 'Y-m-d')
  //     : end.setDate(eventToUpdate.start, true, 'Y-m-d');
  //   sidebar.find(eventLabel).val(eventToUpdate.extendedProps.calendar).trigger('change');
  //   eventToUpdate.extendedProps.location !== undefined ? eventLocation.val(eventToUpdate.extendedProps.location) : null;
  //   eventToUpdate.extendedProps.guests !== undefined
  //     ? eventGuests.val(eventToUpdate.extendedProps.guests).trigger('change')
  //     : null;
  //   eventToUpdate.extendedProps.guests !== undefined
  //     ? calendarEditor.val(eventToUpdate.extendedProps.description)
  //     : null;

  //   //  Delete Event
  //   btnDeleteEvent.on('click', function () {
  //     eventToUpdate.remove();
  //     // removeEvent(eventToUpdate.id);
  //     sidebar.modal('hide');
  //     $('.event-sidebar').removeClass('show');
  //     $('.app-calendar .body-content-overlay').removeClass('show');
  //   });
  // }

  function eventClick(info) {
    var event = info.event;
    // Retrieve start date and time of the event
    var startDate = event.start;
    var formattedDate = moment(startDate).format('YYYY-MM-DD');
    var formattedTime = moment(startDate).format('HH:mm:ss');
  
    resetValues();
    $('#loader').show();
    $('.showDetails').html('');
    var endpoint = base_url + '/appointment/getDetailsForWeek';
    var token = $("input[name='_token']").val();

    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'selectedDate': formattedDate,
            'selectedTime': formattedTime,
            'timeZone': jQuery("input[name='timeZone']").val(),
        },
        dataType: "json",
        success: function (data) {
            $('#loader').hide();
            if (data.status == 'Error') {
                toastr.error(data.message, data.status);
            } else {
                var len = data.data.length;
                var res = data.data;
                var div = "";
                for (var i = 0; i < len; i++) {
                    div += '<div class="apt_details"><div class="card shadow-none mb-1 p-1"><h4 class="card-title text-center">' + res[i].request_type + '</h4><div class="row"><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12"><div class="card-image m-auto"><img class="" src="' + res[i].generator_image + '" alt="Card image cap" /></div></div><div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><p><b>Customer: </b>';

                    if (res[i].company_name != null && res[i].company_name != '') {
                        div += '' + res[i].company_name + '';
                    } else {
                        div += '' + res[i].customer_fname + ' ' + res[i].customer_lname + '';
                    }
                    if(res[i].service_id !="4"){
                      div += '<br/><b>Generator: </b>' + res[i].generator_name + '</p></div>';
                    }else{
                       div += '<br/><b>Generator: </b>' + '</p></div>';
                    }

                    div += '<div class="col-12"><b>Slot Time: </b>' + res[i].slot_time + '</div>';

                    if(res[i].service_id !="4"){
                      div += '<div class="col-12"><p class="mb-0"><b>Address: </b>' + res[i].address_line_1 + ', ';

                      div += '' + res[i].state_name + ', ' + res[i].city_name + ', ' + res[i].zip_code + '</p>';
                    }else{
                       div += '<div class="col-12"><p class="mb-0"><b>Address: </b> ';
                    }

                    div += '</div></div></div></div>';
                }
                $('.showDetails').append(div);
            }
        }
    });
  }

  // Modify sidebar toggler
  function modifyToggler() { 
    $('.fc-sidebarToggle-button')
      .empty()
      .append(feather.icons['menu'].toSvg({ class: 'ficon' }));
  }

  // Selected Checkboxes
  function selectedCalendars() {
    var selected = [];
    $('.calendar-events-filter input:checked').each(function () {
      selected.push($(this).attr('data-value'));
    });
    return selected;
  }

  // --------------------------------------------------------------------------------------------------
  // AXIOS: fetchEvents
  // * This will be called by fullCalendar to fetch events. Also this can be used to refetch events.
  // --------------------------------------------------------------------------------------------------
  function fetchEvents(info, successCallback,failureCallback) {

        var calendars = selectedCalendars();
        // We are reading event object from app-calendar-events.js file directly by including that file above app-calendar file.
        // You should make an API call, look into above commented API call for reference
        selectedEvents = events.filter(function (event) {
        // console.log(event.extendedProps.calendar.toLowerCase());
        return calendars.includes(event.extendedProps.calendar.toLowerCase());
        });
        // if (selectedEvents.length > 0) {
        successCallback(selectedEvents);
        // }

  }

  var appointment_calendar = new FullCalendar.Calendar(calendarApt, {
    initialView: 'dayGridMonth',
    height: 'auto',
    events: JSON.parse($('#appointmentCountMonth').val()),
    editable: true,
    dragScroll: true,
    dayMaxEvents: 2,
    eventResizableFromStart: true,
    customButtons: {
      sidebarToggle: {
        text: 'Sidebar'
      }
    },
    headerToolbar: {
      start: 'sidebarToggle, prev,next, title',
      end: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
    },
    views: {
      timeGridWeek: {
        slotMinTime: '08:00:00', // Set the custom start time to 8:00 AM
        slotMaxTime: '18:00:00' // Set the custom end time to 6:00 PM
      },
      timeGridDay : {
        slotMinTime: '08:00:00', // Set the custom start time to 8:00 AM
        slotMaxTime: '18:00:00' // Set the custom end time to 6:00 PM
      }
    },
    selectable: true,
    unselectAuto: false,
    direction: direction,
    initialDate: new Date(),
    navLinks: false, // can click day/week names to navigate views
    weekends: true,
    eventClassNames: function ({ event: calendarEvent }) {
      const colorName = calendarsColor[calendarEvent._def.extendedProps.calendar];

      return [
        // Background Color
        'bg-light-' + colorName
      ];
    },
    dateClick: function (info) {
        handleDateClick(info);
    },
    eventClick: function (info) {
      eventClick(info);
    },
    datesSet: function (dateInfo) {
      loadEventsBasedOnView(dateInfo.view.type);
      modifyToggler();
    },
    viewDidMount: function () {
        modifyToggler();
      },
    
  });

  function handleWeekClick(weekInfo) {
    $('.showDetails').html('');
    $('#loader').show();
    var endpoint = base_url + '/appointment/get-details-by-selected-week';
    var token = $("input[name='_token']").val();

    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'Date': weekInfo,
            'timeZone': jQuery("input[name='timeZone']").val(),
        },
        dataType: "json",
        success: function (data) {
          $('.showDetails').html('');
            $('#loader').hide();
            if (data.status == 'Error') {
                toastr.error(data.message, data.status);
            } else {
                var len = data.data.length;
                var res = data.data;
                var div = "";
                for (var i = 0; i < len; i++) {
                    div += '<div class="apt_details"><div class="card shadow-none mb-1 p-1"><h4 class="card-title text-center">' + res[i].request_type + '</h4><div class="row"><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12"><div class="card-image m-auto"><img class="" src="' + res[i].generator_image + '" alt="Card image cap" /></div></div><div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><p><b>Customer: </b>';

                    if (res[i].company_name != null && res[i].company_name != '') {
                        div += '' + res[i].company_name + '';
                    } else {
                        div += '' + res[i].customer_fname + ' ' + res[i].customer_lname + '';
                    }
                    if(res[i].service_id !="4"){
                      div += '<br/><b>Generator: </b>' + res[i].generator_name + '</p></div>';
                    }else{
                       div += '<br/><b>Generator: </b>' + '</p></div>';
                    }

                    div += '<div class="col-12"><b>Slot Time: </b>' + res[i].slot_time + '</div>';

                    if(res[i].service_id !="4"){
                      div += '<div class="col-12"><p class="mb-0"><b>Address: </b>' + res[i].address_line_1 + ', ';

                      div += '' + res[i].state_name + ', ' + res[i].city_name + ', ' + res[i].zip_code + '</p>';
                    }else{
                       div += '<div class="col-12"><p class="mb-0"><b>Address: </b> ';
                    }

                    div += '</div></div></div></div>';
                }
                $('.showDetails').append(div);
            }
        }
    });
  }

  function loadEventsBasedOnView(viewType) {
    var eventData;
    switch (viewType) {
      case 'dayGridMonth':
        $('.showDetails').html('');
        eventData = JSON.parse($('#appointmentCountMonth').val());
        // Trigger dateClick for the current date after the calendar is rendered
        // var currentDate = appointment_calendar.getDate();
        // handleDateClick({ date: currentDate });
        var startOfWeek = appointment_calendar.view.activeStart;
        var endOfWeek = appointment_calendar.view.activeEnd;
        handleWeekClick({ start: startOfWeek, end: endOfWeek });
        break;
      case 'timeGridWeek':
        $('.showDetails').html('');
        eventData = JSON.parse($('#appointmentCountWeek').val());

        var startOfWeek = appointment_calendar.view.activeStart;
        var endOfWeek = appointment_calendar.view.activeEnd;
        handleWeekClick({ start: startOfWeek, end: endOfWeek });
        break;
      case 'timeGridDay':
        $('.showDetails').html('');
        eventData = JSON.parse($('#appointmentCountWeek').val());

        // Trigger dateClick for the current date after the calendar is rendered
        var currentDate = appointment_calendar.getDate();
        handleDateClick({ date: currentDate });

        break;
      case 'listMonth':
        $('.showDetails').html('');
        eventData = JSON.parse($('#appointmentCountWeek').val());
        break;
    }
    appointment_calendar.removeAllEvents(); // Clear existing events
    appointment_calendar.addEventSource(eventData); // Add new events
  }

  // Render calendar
  appointment_calendar.render();
  // Modify sidebar toggler
  modifyToggler();
  // updateEventClass();


  function handleDateClick(info) {
    var date = moment(info.date).format('YYYY-MM-DD');
    resetValues();
    $('#loader').show();
    $('.showDetails').html('');
    var endpoint = base_url + '/appointment/getDetailsByDate';
    var token = $("input[name='_token']").val();

    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'selectedDate': date,
            'timeZone': jQuery("input[name='timeZone']").val(),
        },
        dataType: "json",
        success: function (data) {
            $('#loader').hide();
            if (data.status == 'Error') {
                toastr.error(data.message, data.status);
            } else {
                var len = data.data.length;
                var res = data.data;
                var div = "";
                for (var i = 0; i < len; i++) {
                    div += '<div class="apt_details"><div class="card shadow-none mb-1 p-1"><h4 class="card-title text-center">' + res[i].request_type + '</h4><div class="row"><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12"><div class="card-image m-auto"><img class="" src="' + res[i].generator_image + '" alt="Card image cap" /></div></div><div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><p><b>Customer: </b>';

                    if (res[i].company_name != null && res[i].company_name != '') {
                        div += '' + res[i].company_name + '';
                    } else {
                        div += '' + res[i].customer_fname + ' ' + res[i].customer_lname + '';
                    }
                    if(res[i].service_id !="4"){
                      div += '<br/><b>Generator: </b>' + res[i].generator_name + '</p></div>';
                    }else{
                       div += '<br/><b>Generator: </b>' + '</p></div>';
                    }

                    div += '<div class="col-12"><b>Slot Time: </b>' + res[i].slot_time + '</div>';

                    if(res[i].service_id !="4"){
                      div += '<div class="col-12"><p class="mb-0"><b>Address: </b>' + res[i].address_line_1 + ', ';

                      div += '' + res[i].state_name + ', ' + res[i].city_name + ', ' + res[i].zip_code + '</p>';
                    }else{
                       div += '<div class="col-12"><p class="mb-0"><b>Address: </b> ';
                    }

                    div += '</div></div></div></div>';
                }
                $('.showDetails').append(div);
            }
        }
    });
  }


  // Validate add new and update form
  if (eventForm.length) {
    eventForm.validate({
      submitHandler: function (form, event) {
        event.preventDefault();
        if (eventForm.valid()) {
          sidebar.modal('hide');
        }
      },
      title: {
        required: true
      },
      'start-date': {
        required: true
      },
      'end-date': {
        required: true
      }
    });
  }

  // Sidebar Toggle Btn
  if (toggleSidebarBtn.length) {
    toggleSidebarBtn.on('click', function () {
      cancelBtn.removeClass('d-none');
    });
  }

  // ------------------------------------------------
  // addEvent
  // ------------------------------------------------
  function addEvent(eventData) {
    calendar.addEvent(eventData);
    calendar.refetchEvents();
  }

  // ------------------------------------------------
  // updateEvent
  // ------------------------------------------------
  function updateEvent(eventData) {
    var propsToUpdate = ['id', 'title', 'url'];
    var extendedPropsToUpdate = ['calendar', 'guests', 'location', 'description'];

    updateEventInCalendar(eventData, propsToUpdate, extendedPropsToUpdate);
  }

  // ------------------------------------------------
  // removeEvent
  // ------------------------------------------------
  function removeEvent(eventId) {
    removeEventInCalendar(eventId);
  }

  // ------------------------------------------------
  // (UI) updateEventInCalendar
  // ------------------------------------------------
  const updateEventInCalendar = (updatedEventData, propsToUpdate, extendedPropsToUpdate) => {
    const existingEvent = calendar.getEventById(updatedEventData.id);

    // --- Set event properties except date related ----- //
    // ? Docs: https://fullcalendar.io/docs/Event-setProp
    // dateRelatedProps => ['start', 'end', 'allDay']
    // eslint-disable-next-line no-plusplus
    for (var index = 0; index < propsToUpdate.length; index++) {
      var propName = propsToUpdate[index];
      existingEvent.setProp(propName, updatedEventData[propName]);
    }

    // --- Set date related props ----- //
    // ? Docs: https://fullcalendar.io/docs/Event-setDates
    existingEvent.setDates(updatedEventData.start, updatedEventData.end, { allDay: updatedEventData.allDay });

    // --- Set event's extendedProps ----- //
    // ? Docs: https://fullcalendar.io/docs/Event-setExtendedProp
    // eslint-disable-next-line no-plusplus
    for (var index = 0; index < extendedPropsToUpdate.length; index++) {
      var propName = extendedPropsToUpdate[index];
      existingEvent.setExtendedProp(propName, updatedEventData.extendedProps[propName]);
    }
  };

  // ------------------------------------------------
  // (UI) removeEventInCalendar
  // ------------------------------------------------
  function removeEventInCalendar(eventId) {
    calendar.getEventById(eventId).remove();
  }

  // Add new event
  $(addEventBtn).on('click', function () {
    if (eventForm.valid()) {
      var newEvent = {
        id: calendar.getEvents().length + 1,
        title: eventTitle.val(),
        start: startDate.val(),
        end: endDate.val(),
        startStr: startDate.val(),
        endStr: endDate.val(),
        display: 'block',
        extendedProps: {
          location: eventLocation.val(),
          guests: eventGuests.val(),
          calendar: eventLabel.val(),
          description: calendarEditor.val()
        }
      };
      if (eventUrl.val().length) {
        newEvent.url = eventUrl.val();
      }
      if (allDaySwitch.prop('checked')) {
        newEvent.allDay = true;
      }
      addEvent(newEvent);
    }
  });

  // Update new event
  updateEventBtn.on('click', function () {
    if (eventForm.valid()) {
      var eventData = {
        id: eventToUpdate.id,
        title: sidebar.find(eventTitle).val(),
        start: sidebar.find(startDate).val(),
        end: sidebar.find(endDate).val(),
        url: eventUrl.val(),
        extendedProps: {
          location: eventLocation.val(),
          guests: eventGuests.val(),
          calendar: eventLabel.val(),
          description: calendarEditor.val()
        },
        display: 'block',
        allDay: allDaySwitch.prop('checked') ? true : false
      };

      updateEvent(eventData);
      sidebar.modal('hide');
    }
  });

  // Reset sidebar input values
  function resetValues() {
    endDate.val('');
    eventUrl.val('');
    startDate.val('');
    eventTitle.val('');
    eventLocation.val('');
    allDaySwitch.prop('checked', false);
    eventGuests.val('').trigger('change');
    calendarEditor.val('');
  }


  // When modal hides reset input values
  sidebar.on('hidden.bs.modal', function () {
    resetValues();
  });

  // Hide left sidebar if the right sidebar is open
  $('.btn-toggle-sidebar').on('click', function () {
    btnDeleteEvent.addClass('d-none');
    updateEventBtn.addClass('d-none');
    addEventBtn.removeClass('d-none');
    $('.app-calendar-sidebar, .body-content-overlay').removeClass('show');
  });

  // Select all & filter functionality
  if (selectAll.length) {
    selectAll.on('change', function () {
      var $this = $(this);

      if ($this.prop('checked')) {
        calEventFilter.find('input').prop('checked', true);
      } else {
        calEventFilter.find('input').prop('checked', false);
      }
      calendar.refetchEvents();
    });
  }

  if (filterInput.length) {
    filterInput.on('change', function () {
      $('.input-filter:checked').length < calEventFilter.find('input').length
        ? selectAll.prop('checked', false)
        : selectAll.prop('checked', true);
      calendar.refetchEvents();
    });
  }
});
