$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function(value, element) {
            if(value.trim() ==''){
                return false;
            }
            return true;
        }, "This field is required.");
        
        jqForm.validate({
            rules: {
                'office_hours': {
                    required: true,
                    require_field: true,
                },
                'slot_time': {
                    required: true,
                    require_field: true,
                },
                'lead_day': {
                    required: true,
                    require_field: true,
                },
                'cancellation_charge': {
                    required: true,
                    require_field: true,
                },
                'hours_before_cancel_request': {
                    required: true,
                    require_field: true,
                },
                'hours_before_cancel_request': {
                    required: true,
                    require_field: true,
                },
            },
            messages: {
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});