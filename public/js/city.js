// Datatable
$(document).ready(function () {
    if (document.getElementById("table_city")) {
        var table = $('#table_city').DataTable({
            processing: true,
            serverSide: true,
            // order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/city/list",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.status = jQuery("#status").val();
                    data.country = jQuery("#country_id").val();
                    data.state = jQuery("#state_id").val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', orderable: true, searchable: true},
                { data: 'state',name: 'states.name',orderable: true, searchable: true},
                { data: 'country',name: 'countries.name',orderable: true, searchable: true},
                { data: 'city_status', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });
        $('#status').on('change', function () {
            table.draw();
        })
        $('#country_id').on('change', function () {
            table.draw();
        })
        $('#state_id').on('change', function () {
            table.draw();
        })
    }
   

    $(function () {
        var jqForm = $('#jquery-val-form');
        if (jqForm.length) {
            jqForm.validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    element.closest('.form-group').append(error);
                },
                submitHandler: function (form) {
                    $('#loader').show(); 
                    form.submit();
                  }
            });
        }
    });
});