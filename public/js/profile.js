$(document).ready(function() {

    var jqForm1 = $('#change_password');
    if (jqForm1.length) {
        // jQuery.validator.addMethod("notEqual", function(value, element, param) {
        //     return this.optional(element) || value != $(param).val();
        // }, "This has to be different...");

        $.validator.addMethod( "notEqualTo", function( value, element, param ) {
            return this.optional( element ) || !$.validator.methods.equalTo.call( this, value, element, param );
        }, "Old and New Password should be different." );
    
        $("#change_password").validate({            
          rules: {
            old_password: {
                required: true,
                nospaces: true,
                minlength: 8,
                maxlength: 30,
            },
            password: {
                required: true,
                nospaces: true,
                notEqualTo: '#old_password',
                minlength: 8,
                maxlength: 30,
                strongPassword:true,
            },
            password_confirmation: {
              required: true,
              nospaces: true,
              equalTo : "#password",              
              minlength: 8,
              maxlength: 30,
            }

          },
          messages:{
            password_confirmation: {
                equalTo: 'New Password and Confirm Password  should be same.',
            }
          },
          errorElement: 'span',
          errorPlacement: function (error, element) {
              element.closest('.form-group').append(error);
          },
          submitHandler: function (form) {
            $('#loader').show();
            form.submit();
        }
        });
    }
  
    $(function () {
        var jqForm = $('#edit-profile');
        if (jqForm.length) {
            jQuery.validator.addMethod("validate_email", function(value, element) {
                if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                    return true;
                } else {
                    return false;
                }
            }, "Please enter valid email");
            jQuery.validator.addMethod("require_field", function(value, element) {
                if(value.trim() ==''){
                    return false;
                }
                return true;
            }, "This field is required.");
            jQuery.validator.addMethod("phoneNoValidation", function(value, element) {
                if(value == "(000)000-0000" || value == "0000000000"){
                return false;
                }
                return true;
            }, "* Please enter valid phone no");
            jQuery.validator.addMethod("strongPassword", function(value) {
                return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
            },"The password must contain at least 1 number, 1 lower case letter,1 upper case letter and 1 special symbol"); 

            jqForm.validate({
                rules: {
                    'user_fname': {
                        maxlength:50,
                        require_field: true,
                    },
                    'user_lname': {
                        maxlength:50,
                        require_field: true,
                    },
                    'email': {
                        required: true,
                        maxlength:50,
                        email: true,
                        validate_email: true,
                        require_field: true,
                    },
                    'password': {
                        minlength:8,
                        maxlength:50,
                        required: true,
                        require_field: true,
                        strongPassword:true,
                    },
                    'user_phone_no': {
                        phoneNoValidation:true,
                        required: true,
                    },
                    'notifications[]' : {
                        required : true
                    }
                },
                messages: {
                    'user_phone_no': {
                        phoneNoValidation:"Please enter valid phone no",
                        minlength :"Please enter at least 10 characters." 
                    }
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    element.closest('.form-group').append(error);
                }
            });
        }
    });
});