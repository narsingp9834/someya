$(document).ready(function () {
    if (document.getElementById("table_address")) {
        var table = $('#table_address').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url : base_url + "/customer/list",
                data: function ( data ) {
                    data.timeZone   = jQuery("input[name='timeZone']").val();
                    data.status     = jQuery('#status').val();
                    data.type     = jQuery('#type').val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'type', name: 'customers.type'},
                {data: 'user_fname'},
                {data: 'user_lname'},
                {data: 'email'},
                {data: 'user_phone_no'},
                {data: 'created_at', searchable: false},
                {data: 'user_status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        // / Filter data status wise
        jQuery('#status').on('change', function(){
            table.draw();
        });

        jQuery('#type').on('change', function(){
            table.draw();
        });
    } 
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        
        jqForm.validate({
            rules: {
            },
            messages: {
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            },
        });
    }
});