<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CMSController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuickLinkController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Page Route
// Route::group(['middleware' => ['httpsProtocol']], function () {
    Route::get('/not-authorized',       [CommonController::class, 'notAuthorized']);
    // Route::get('appointment/confirm/{id}',               [AppointmentController::class, 'confirmAppointment']);
    // Auth routes
    Route::get('/', [DashboardController::class, 'showHomePage']);
    Auth::routes(['verify' => true]);
    Route::get('logout', [LoginController::class, 'logout']);
    // Dashboard routes
    Route::get('/home', [ProfileController::class, 'dashboard'])->name('home');
    Route::post('/checkEmail',      [CommonController::class, 'checkEmail']);

    Route::get('/admin', [LoginController::class, 'showLoginForm']);
    Route::get('/login-with-otp', [LoginController::class, 'loginWithOtp']);
    Route::get('/seller/register', [LoginController::class, 'sellerRegister']);
    Route::post('/send-otp', [LoginController::class, 'sendOtp']);
    Route::post('/verify-otp', [LoginController::class, 'verifyOtp']);
    Route::get('/seller/list', [DashboardController::class, 'sellerList']);
    Route::get('/seller/loginUser/{id}', [DashboardController::class, 'loginUser']);

    //Profile 
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [ProfileController::class, 'editProfile']);
        Route::post('/update', [ProfileController::class, 'update']);
    });

    // Common routes
    Route::group(['prefix' => 'common'], function () {
        Route::post('/delete',           [CommonController::class, 'destroyRecord']);
        Route::post('/status',           [CommonController::class, 'updateStatus']);
        Route::post('/getCityById',      [CommonController::class, 'getCities']);
    });

    
    Route::get('/contact-us', function(){
        return view('contact_us');
     });


    //  Route::post('/submit-contact-us', [DashboardController::class, 'submitContactUs']); 
    Route::post('/submit-contact-us', [DashboardController::class, 'submitContactUs']);  
    Route::get('/enquiry', [DashboardController::class, 'Enquiry']);
    Route::get('/our-quality', [DashboardController::class, 'ourQuality']);
    Route::get('/why-us', [DashboardController::class, 'whyUs']);
    
    Route::group(['prefix' => 'banner'], function () {
        Route::get('/',     [DashboardController::class, 'banner']);
        Route::post('/store',    [DashboardController::class, 'bannerStore']);
    });

    Route::group(['prefix' => 'customer-banner'], function () {
        Route::get('/',     [DashboardController::class, 'customerBanner']);
        Route::post('/store',    [DashboardController::class, 'customerBannerStore']);
    });

    Route::group(['prefix' => 'quick-link'], function () {
        Route::get('/',     [QuickLinkController::class, 'index']);
        Route::get('/create',     [QuickLinkController::class, 'create']);
        Route::post('/store',    [QuickLinkController::class, 'store']);
        Route::get('/edit/{id}', [QuickLinkController::class, 'edit']);
        Route::post('/update',  [QuickLinkController::class, 'update']);
        Route::get('/view/{id}', [QuickLinkController::class, 'view']);
    });

    Route::group(['prefix' => 'cms-product'], function () {
        Route::get('/',     [CMSController::class, 'productIndex']);
        Route::get('/create',     [CMSController::class, 'productCreate']);
        Route::post('/store',    [CMSController::class, 'productStore']);
        Route::get('/edit/{id}', [CMSController::class, 'productEdit']);
        Route::post('/update',  [CMSController::class, 'productUpdate']);
        Route::get('/view/{id}', [CMSController::class, 'productView']);
    });


     //Change Password 
     Route::group(['prefix' => 'change-password'], function () {
        Route::get('/', [ProfileController::class, 'changePassword']);
        Route::post('/update', [ProfileController::class, 'updatePassword']);
    });

    Route::get('/t_c', function(){
        return view('terms_conditions');
     });

     Route::get('/disclaimers', function(){
        return view('disclaimers');
     }); 
     
     Route::get('/privacy_policy', function(){
        return view('privacy_policy');
     }); 
     
     Route::get('/enquiry', [QuickLinkController::class, 'Enquiry']); 
     Route::get('/contact-us', [DashboardController::class, 'contactUs']); 
     

// });


